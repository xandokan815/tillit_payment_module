<?php

namespace Tillit\Gateway\Setup;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\TestFramework\Event\Magento;
use Tillit\Gateway\Model\Tillit as TILLIT;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    protected $quoteSetupFactory;

    /**
     * @var SalesSetupFactory
     */
    protected $salesSetupFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    protected $setup;

    /**
     * @param EavSetupFactory $eavSetupFactory
     * @param QuoteSetupFactory $quoteSetupFactory
     * @param SalesSetupFactory $salesSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        QuoteSetupFactory $quoteSetupFactory,
        SalesSetupFactory $salesSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install( ModuleDataSetupInterface $setup, ModuleContextInterface $context )
    {
        $installer = $setup;
        $this->setup = $setup;

        $installer->startSetup();

        /**
         * Install order statuses from config
         */
        $data = [];
        $statuses = [
            TILLIT::STATUS_NEW => __('TILLIT New Order'),
            TILLIT::STATUS_FAILED => __('TILLIT Failed'),
        ];

        foreach ($statuses as $code => $info) {
            $data[] = ['status' => $code, 'label' => $info];
        }

        $installer->getConnection()->insertArray(
            $installer->getTable('sales_order_status'),
            ['status', 'label'],
            $data
        );


        /**
         * Install order states from config
         */
        $data = [];
        $states = [
            'new' => [
                'statuses' => [ TILLIT::STATUS_NEW ],
            ],
            'canceled' => [
                'statuses' => [ TILLIT::STATUS_FAILED ],
            ],
        ];

        foreach ($states as $code => $info) {
            if (isset($info['statuses'])) {
                foreach ($info['statuses'] as $status) {
                    $data[] = [
                        'status' => $status,
                        'state' => $code,
                        'is_default' => 0,
                        'visible_on_front' => 1,
                    ];
                }
            }
        }
        $installer->getConnection()->insertArray(
            $installer->getTable('sales_order_status_state'),
            ['status', 'state', 'is_default', 'visible_on_front'],
            $data
        );
        $fields = [['key' => 'company_id', 'label'=>__('Company Id')],
        ['key' => 'account_type', 'label'=>__('Account Type')],
        ['key' => 'department', 'label'=>__('Department')],
        ['key' => 'project', 'label'=>__('Project')],
        ['key' => 'company_name', 'label'=>__('Tillit Company Name')]];

        
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        foreach($fields as $key=>$value)
        {
            if($value['key'] == "company_name") {
                $eavSetup->addAttribute(
                    AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
                    $value['key'],
                    $this->getCompanyName($value['label'])
                );
            }else{
                $eavSetup->addAttribute(
                    AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
                    $value['key'],
                    $this->getField($value['label'])
                );
            }
            
            $this->quoteSetupFactory->create()->addAttribute(
                'quote_address',
                $value['key'],
                ['type' => Table::TYPE_TEXT]
            );
            $this->salesSetupFactory->create()->addAttribute(
                'order_address',
                $value['key'],
                ['type' => Table::TYPE_TEXT]
            );
    
            $this->addAttributeToAllForm($eavSetup->getAttributeId(
                AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
                $value['key']
            ));
        }


        $installer->endSetup();
    }
    protected function getField($label)
    {
        return [
            'label' => $label,
            'type' => 'varchar',
            'input' => 'text',
            'required' => false,
            'sort_order' => 50,
            'position' => 50,
            'system' => false,
            'is_user_defined',
            'visible' => true,
        ];
    }
    
    protected function getCompanyName($label)
    {
        return [
            'label' => $label,
            'type' => 'varchar',
            'input' => 'text',
            'required' => false,
            'sort_order' => 50,
            'position' => 50,
            'system' => false,
            'is_user_defined',
            'visible' => true,
        ];
    }
    protected function addAttributeToAllForm($attributeId)
    {
        foreach ([
                    'adminhtml_customer_address',
                    'customer_address_edit',
                    'customer_register_address'
                ] as $formCode) {
            $this->setup->getConnection()
                ->insertMultiple(
                    $this->setup->getTable('customer_form_attribute'),
                    ['form_code' => $formCode, 'attribute_id' => $attributeId]
                );
        }
    }
}
