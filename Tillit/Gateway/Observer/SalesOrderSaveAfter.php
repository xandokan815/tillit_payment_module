<?php

namespace Tillit\Gateway\Observer;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Tillit\Gateway\Helper\Config;


class SalesOrderSaveAfter implements ObserverInterface
{

    protected $orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Tillit\Gateway\Helper\Config $data,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $transactions,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->curlClient = $curl;
        $this->helper = $data;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->transactions = $transactions;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $order_id = $order->getId();
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodCode = $method->getCode();
        $addition_data = $payment->getAdditionalInformation();
        if($methodCode == "tillit_payment")
        { 
            try {
                if($order->getState() == "complete" && (!isset($addition_data['marked_completed']) || $addition_data['marked_completed'] == false) )
                {
                    if($this->helper->getEnableFinalizePurchase() == '1')
                    {
                        $tillit_order_id = $order->getTillitOrderId();


                        // Change the order status
                        $response = $this->helper->make_request("/v1/order/${tillit_order_id}/fulfilled");

                        $response = json_decode($response, true);
                        if(!isset($response)) {
                            $order->addStatusHistoryComment(sprintf(__('Could not update status to fulfilled on Tillit, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                            return $this;
                        }
                
                        $tillit_err = $this->helper->get_tillit_error_msg($response);
                        if ($tillit_err) {
                            $order->addStatusHistoryComment(sprintf(__('Could not update status to fulfilled on Tillit, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                            return $this;
                        }

                        // Save invoice number
                        if (isset($response['payment']) && $response['payment']['payment_details'] && $response['payment']['payment_details']['invoice_number']) {
                        
                            $addition_data['marked_completed'] = true;
                            $payment->setAdditionalInformation($addition_data);
                            $payment->save();
                            $order->addStatusHistoryComment(sprintf(__('Tillit Order marked as completed with invoice number  %s'), $response['payment']['payment_details']['invoice_number']))->save();
                            return $this;
                        }
                    }
                }
                
            } catch (LocalizedException $e) {
            
            }

        }
        return $this;
    }
}