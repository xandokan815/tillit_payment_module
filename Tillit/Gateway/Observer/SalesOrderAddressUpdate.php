<?php

namespace Tillit\Gateway\Observer;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Tillit\Gateway\Helper\Config;


class SalesOrderAddressUpdate implements ObserverInterface
{

    protected $orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Tillit\Gateway\Helper\Config $data,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $transactions,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->curlClient = $curl;
        $this->helper = $data;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->transactions = $transactions;
        $this->_logger = $logger;
        $this->_orderFactory = $orderFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order_id = $observer->getEvent()->getOrderId();
        $order = $this->_orderFactory->create()->load($order_id);
        if($order)
        {
            $payment = $order->getPayment();
            $method = $payment->getMethodInstance();
            $methodCode = $method->getCode();
            if($methodCode == "tillit_payment")
            { 
                try {
                
                    $original_orders_data = $payment->getAdditionalInformation();
            
                    if(isset($original_orders_data['gateway_data']['original_order_id']))
                    {
                        $tillit_meta = $this->helper->get_save_tillit_meta($order);
                        if (!$tillit_meta) return $this;

                        $this->helper->update_tillit_order($order);
                        
                        return $this;
                    }else{
                        return $this;
                    }
                } catch (LocalizedException $e) {
                
                }
    
            }
        }
        return $this;
    }
}