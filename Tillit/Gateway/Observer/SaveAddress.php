<?php
namespace Tillit\Gateway\Observer;

class SaveAddress implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\DataObject\Copy $objectCopyService
     */
    public function __construct(
        \Magento\Framework\DataObject\Copy $objectCopyService,
        \Tillit\Gateway\Helper\Config $helper 
    ) {
        $this->objectCopyService = $objectCopyService;
        $this->helper = $helper;
    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var OrderPaymentInterface $payment */
        $payment = $observer->getDataByKey('payment');

        $order = $payment->getOrder();

        $shippingAddress = $order->getShippingAddress();
        $billingAddress = $order->getBillingAddress();
        $objectManager          = \Magento\Framework\App\ObjectManager::getInstance();
        $addressRepository      = $objectManager->create('\Magento\Customer\Api\AddressRepositoryInterface');
        $addressFactory         = $objectManager->create('\Magento\Customer\Model\AddressFactory');


        $addressRepository      = $objectManager->create('\Magento\Customer\Api\AddressRepositoryInterface');
        if($order->getShippingAddress() && $order->getShippingAddress()->getCustomerAddressId())
        {
            $addressObject = $addressRepository->getById($order->getShippingAddress()->getCustomerAddressId());
            $addressObject->setCustomAttribute('account_type', $shippingAddress->getAccountType());
            $addressObject->setCustomAttribute('company_id', $shippingAddress->getCompanyId());
            $addressObject->setCustomAttribute('company_name', $shippingAddress->getCompanyName());
            $addressObject->setCustomAttribute('department', $shippingAddress->getDepartment());
            $addressObject->setCustomAttribute('project', $shippingAddress->getProject());
            $addressRepository->save($addressObject);
        }

        if($order->getBillingAddress() && $order->getBillingAddress()->getCustomerAddressId())
        {
            $addressObject2    = $addressRepository->getById($order->getBillingAddress()->getCustomerAddressId());
            $addressObject2->setCustomAttribute('account_type', $billingAddress->getAccountType());
            $addressObject2->setCustomAttribute('company_id', $billingAddress->getCompanyId());
            $addressObject2->setCustomAttribute('company_name', $billingAddress->getCompanyName());
            $addressObject2->setCustomAttribute('department', $billingAddress->getDepartment());
            $addressObject2->setCustomAttribute('project', $billingAddress->getProject());
            $addressRepository->save($addressObject2);
        }   
        
    }
}