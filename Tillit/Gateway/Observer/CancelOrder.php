<?php

namespace Tillit\Gateway\Observer;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Tillit\Gateway\Helper\Config;


class CancelOrder implements ObserverInterface
{

    protected $orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Tillit\Gateway\Helper\Config $data,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $transactions,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->curlClient = $curl;
        $this->helper = $data;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->transactions = $transactions;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $order_id = $order->getId();
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodCode = $method->getCode();
        if($methodCode == "tillit_payment")
        { 
            try {
                $tillit_order_id = $order->getTillitOrderId();

                // Change the order status
                $response = $this->helper->make_request("/v1/order/${tillit_order_id}/cancel");

                $response = json_decode($response, true);
                if(!isset($response)) {
                    $order->addStatusHistoryComment(sprintf(__('Could not update status to cancelled, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                    return $this;
                }
        
                $tillit_err = $this->helper->get_tillit_error_msg($response);
                if ($tillit_err) {
                    $order->addStatusHistoryComment(sprintf(__('Could not update status to cancelled, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                    return $this;
                }

                $order->addStatusHistoryComment(sprintf(__('Tillit Order marked as cancelled')))->save();
                return $this;
                
            } catch (LocalizedException $e) {
            
            }

        }
        return $this;
    }
}