<?php

namespace Tillit\Gateway\Observer;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Controller\Result\RedirectFactory;
use Tillit\Gateway\Helper\Config;


class SalesOrderRefund implements ObserverInterface
{

    protected $orderRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Tillit\Gateway\Helper\Config $data,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Sales\Api\Data\TransactionSearchResultInterfaceFactory $transactions,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->curlClient = $curl;
        $this->helper = $data;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->transactions = $transactions;
        $this->_logger = $logger;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $payment = $observer->getEvent()->getPayment();
        $creditMemo = $observer->getEvent()->getCreditmemo();
        $order = $payment->getOrder();
        $order_id = $order->getId();
        $method = $payment->getMethodInstance();
        $methodCode = $method->getCode();
        $invoice = $creditMemo->getInvoice();
        if($methodCode == "tillit_payment" && $creditMemo->getDoTransaction())
        { 
            try {
                if($order->getState() != "complete")
                {
                    throw new \Exception(__('Only Completed order can be refunded by Tillit'));
                }
                $tillit_order_id = $order->getTillitOrderId();
                
                if (!$tillit_order_id) {
                    throw new \Exception(__('Could not initiate refund by Tillit'));
                }
                $response = $this->helper->make_request(
                    "/v1/order/${tillit_order_id}/refund",
                    $this->helper->compose_tillit_refund(
                        $creditMemo,
                        -$creditMemo->getGrandTotal(),
                        $creditMemo->getOrderCurrencyCode(),
                        $this->helper->getEnableRefund() === '1',
                        $order
                    ),
                    'POST'
                );
                $response = json_decode($response, true);
                if(!isset($response)) {
                    $order->addStatusHistoryComment(sprintf(__('Failed to request refund order to Tillit')))->save();
                    return $this;
                }
        
                $tillit_err = $this->helper->get_tillit_error_msg($response);
                if ($tillit_err) {
                    $order->addStatusHistoryComment(sprintf(__('Failed to request refund order to Tillit, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                    throw new \Exception(__('Request refund order to Tillit has errors'));
                }

                if (!$response['amount']) {
                    $order->addStatusHistoryComment(sprintf(__('Failed to refund order by Tillit, please check with Tillit admin for id %s'), $tillit_order_id));
                    throw new \Exception(__('Failed to refund order by Tillit'));

                }
        
                $order->addStatusHistoryComment(sprintf(__('Successfully refunded order Tillit admin for id %s'), $tillit_order_id))->save();
            } catch (LocalizedException $e) {
            
            }

        }
        return $this;
    }
}