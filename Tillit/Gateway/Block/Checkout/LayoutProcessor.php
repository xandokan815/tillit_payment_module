<?php

namespace Tillit\Gateway\Block\Checkout;

class LayoutProcessor
{
    public function __construct(
        \Tillit\Gateway\Helper\Config $helper)
    {
        $this->_helper = $helper;
    }
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array $jsLayout
    ) {
        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['account_type'] = $this->add_account_fields();

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['project'] = $this->add_project_fields();

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['department'] = $this->add_department_fields();

        $jsLayout['components']['checkout']['children']['steps']['children']
        ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
        ['children']['company_id'] = $this->add_company_id_fields();

        if($this->_helper->getEnableCompanyName())
        {
            if(isset($jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children']['company']))
            {
                $company = $jsLayout['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
                ['children']['company'];
                $company['visible'] = false;

                $jsLayout['components']['checkout']['children']['steps']['children']
                ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
                ['children']['company'] = $company;
            }
            $jsLayout['components']['checkout']['children']['steps']['children']
            ['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children']['company_name'] = $this->add_Company_name_fields();
        }

        if(isset($jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['payments-list'])) {
            $paymentForms = $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['payments-list']['children'];

            foreach ($paymentForms as $paymentGroup => $groupConfig) {
                if (isset($groupConfig['component']) AND $groupConfig['component'] === 'Magento_Checkout/js/view/billing-address') {
                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['account_type']= $this->add_billing_account_fields($paymentGroup);

                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['project']= $this->add_billing_project_fields($paymentGroup);

                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['department']= $this->add_billing_department_fields($paymentGroup);

                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company_id']= $this->add_billing_company_id_fields($paymentGroup);

                    if($this->_helper->getEnableCompanyName())
                    {
                        if(isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company']))
                        {
                            $company = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                            ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company'];
                            $company['visible'] = false;

                            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company'] = $company;
                        }
                        $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['company_name'] = $this->add_Billing_Company_name_fields($paymentGroup);
                    }

                }
            }
        }
    
        if(isset($jsLayout['components']['checkout']['children']['steps']['children']
        ['billing-step']['children']['payment']['children']
        ['afterMethods']['children']['billing-address-form'])) {
            $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children'][$customAttributeCode] = $this->getCustomfieldBilling('shared');

            $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['account_type']= $this->add_billing_account_fields('shared');

            $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['project']= $this->add_billing_project_fields('shared');

            $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['department']= $this->add_billing_department_fields('shared');

            $jsLayout['components']['checkout']['children']['steps']['children']
            ['billing-step']['children']['payment']['children']
            ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['company_id']= $this->add_billing_company_id_fields('shared');

            if($this->_helper->getEnableCompanyName())
            {
                if(isset($jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']
                ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['company']))
                {
                    $company = $jsLayout['components']['checkout']['children']['steps']['children']
                    ['billing-step']['children']['payment']['children']
                    ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['company'];
                    $company['visible'] = false;

                    $jsLayout['components']['checkout']['children']['steps']['children']
                        ['billing-step']['children']['payment']['children']
                        ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['company'] = $company;
                }
                $jsLayout['components']['checkout']['children']['steps']['children']
                ['billing-step']['children']['payment']['children']
                ['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['company_name'] = $this->add_Billing_Company_name_fields($paymentGroup);
            }
        }

        
        return $jsLayout;  
    }
    public function add_account_fields()
    {
        $customAttributeCode = 'account_type';
        if($this->_helper->getEnableB2B()) {
            $customField = [
                'component' => 'Tillit_Gateway/js/view/form/element/radio',
                'config' => [
                    'customScope' => 'shippingAddress.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Tillit_Gateway/form/element/radio'
                ],
                'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
                'label' => __('Account Type'),
                'provider' => 'checkoutProvider',
                'sortOrder' => 0,
                'value' => "business",
                'validation' => [
                    'required-entry' => true
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
            ];
        } else {
            $customField = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'shippingAddress.custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/hidden'
                ],
                'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
                'label' => __('Account Type'),
                'provider' => 'checkoutProvider',
                'sortOrder' => 0,
                'validation' => [
                    'required-entry' => true
                ],
                'value' => "business",
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false,
            ];
        }

        return $customField;
    }

    public function add_billing_account_fields($paymentMethodCode)
    {
        $paymentMethodCode = str_replace('-form', '', $paymentMethodCode);
        $customAttributeCode = 'account_type';
        if($this->_helper->getEnableB2B()) {
            $customField = [
                'component' => 'Tillit_Gateway/js/view/form/element/radio',
                'config' => [
                    'customScope' => 'billingAddress'. $paymentMethodCode  . 'custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Tillit_Gateway/form/element/radio'
                ],
                'dataScope' => 'billingAddress'. $paymentMethodCode  .'.custom_attributes' . '.' . $customAttributeCode,
                'label' => __('Account Type'),
                'provider' => 'checkoutProvider',
                'value' => "business",
                'sortOrder' => 0,
                'validation' => [
                    'required-entry' => true
                ],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
            ];
        } else {
            $customField = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' =>  'billingAddress'. $paymentMethodCode  . 'custom_attributes',
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/hidden'
                ],
                'dataScope' =>'billingAddress'. $paymentMethodCode  .'.custom_attributes' . '.' . $customAttributeCode,
                'label' => __('Account Type'),
                'provider' => 'checkoutProvider',
                'sortOrder' => 0,
                'validation' => [
                    'required-entry' => true
                ],
                'value' => "business",
                'filterBy' => null,
                'customEntry' => null,
                'visible' => false,
            ];
        }

        return $customField;
    }

    public function add_company_id_fields()
    {
        $customAttributeCode = 'company_id';
        $customField = [
            'component' => 'Tillit_Gateway/js/view/form/element/department',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'Tillit_Gateway/form/element/department'
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Company ID'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 70,
            'validation' => [
                'required-entry' => true
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }

    public function add_billing_company_id_fields($paymentMethodCode)
    {
        $paymentMethodCode = str_replace('-form', '', $paymentMethodCode);
        $customAttributeCode = 'company_id';
        $customField = [
            'component' => 'Tillit_Gateway/js/view/form/element/department',
            'config' => [
                'customScope' => 'billingAddress'. $paymentMethodCode  . 'custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'Tillit_Gateway/form/element/department'
            ],
            'dataScope' => 'billingAddress'. $paymentMethodCode  .'.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Company ID'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 70,
            'validation' => [
                'required-entry' => true
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }

    public function add_department_fields()
    {
        $customAttributeCode = 'department';
        $customField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Department'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 70,
            'validation' => [
                'required-entry' => false
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }
    public function add_billing_department_fields($paymentMethodCode)
    {
        $paymentMethodCode = str_replace('-form', '', $paymentMethodCode);
        $customAttributeCode = 'department';
        $customField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddress'. $paymentMethodCode  . '.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'billingAddress'. $paymentMethodCode  .'.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Department'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 70,
            'validation' => [
                'required-entry' => false
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }
    public function add_project_fields()
    {
        $customAttributeCode = 'project';
        $customField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Project'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 70,
            'validation' => [
                'required-entry' => false
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }
    public function add_billing_project_fields($paymentMethodCode)
    {
        $paymentMethodCode = str_replace('-form', '', $paymentMethodCode);
        $customAttributeCode = 'project';
        $customField = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'billingAddress'. $paymentMethodCode  . '.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input'
            ],
            'dataScope' => 'billingAddress'. $paymentMethodCode  .'.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Project'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 70,
            'validation' => [
                'required-entry' => false
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }

    public function add_Company_name_fields()
    {
        $customAttributeCode = 'company_name';
        $customField = [
            'component' => 'Tillit_Gateway/js/view/form/element/company-select',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'Tillit_Gateway/form/element/select2',
                'id' => $customAttributeCode,
                'additionalClasses' => 'custom-select-wrap',
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Company Name'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 60,
            'validation' => [
                'required-entry' => true
            ],
            'options' => [
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }
    public function add_Billing_Company_name_fields($paymentMethodCode)
    {
        $paymentMethodCode = str_replace('-form', '', $paymentMethodCode);
        $customAttributeCode = 'company_name';
        $customField = [
            'component' => 'Tillit_Gateway/js/view/form/element/company-select',
            'config' => [
                'customScope' => 'billingAddress'. $paymentMethodCode  . '.custom_attributes',
                'customEntry' => null,
                'template' => 'ui/form/field',
                'elementTmpl' => 'Tillit_Gateway/form/element/select2',
                'id' => $customAttributeCode,
                'additionalClasses' => 'custom-select-wrap',
            ],
            'dataScope' => 'billingAddress'. $paymentMethodCode  .'.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Company Name'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 60,
            'validation' => [
                'required-entry' => true
            ],
            'options' => [
            ],
            'filterBy' => null,
            'customEntry' => null,
            'visible' => true,
        ];

        return $customField;
    }
}
