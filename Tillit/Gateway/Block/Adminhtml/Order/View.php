<?php
namespace Tillit\Gateway\Block\Adminhtml\Order;

class View extends \Magento\Backend\Block\Template
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->coreRegistry = $registry;
    }

    public function getAdditionalData()
    {
        $order = $this->_getOrder();
        $payment = $order->getPayment();
        $addition_data = $payment->getAdditionalInformation();
        return $addition_data;
    }

    public function getInvoiceUrl($data)
    {
        return (isset($data['invoice_url'])) ? $data['invoice_url'] : '';
    }

    public function getTillitOrderId($data)
    {
        return (isset($data['gateway_data']['original_order_id'])) ? $data['gateway_data']['original_order_id'] : '';
    }

    public function getMethod()
    {
        $order = $this->_getOrder();
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodCode = $method->getCode();
        return $methodCode;
    }
    /**
     * Retrieve order model instance
     *
     * @return Order
     */
    protected function _getOrder()
    {
        return $this->coreRegistry->registry('sales_order');
    }
}