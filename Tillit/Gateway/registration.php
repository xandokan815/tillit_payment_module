<?php
/**
 * Copyright © 2021
 *
 * @author Gang Jin <jgang@tillit.ai>
 * @package Tillit_Gateway
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Tillit_Gateway',
    __DIR__
);
