<?php

namespace Tillit\Gateway\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Tillit\Gateway\Model\Tillit as TILLIT;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;
class Config extends \Magento\Payment\Helper\Data
{
    protected $scopeConfig;
    
    const CODE = 'tillit_payment';

    const XML_PATH_ENABLED = 'payment/tillit_payment/active';
    const XML_PATH_LOGO = 'payment/tillit_payment/merchant_logo';
    const XML_PATH_TITLE= 'payment/tillit_payment/title';
    const XML_PATH_SUBTITLE = 'payment/tillit_payment/subtitle';
    const XML_PATH_MERCHANT_ID = 'payment/tillit_payment/tillit_merchant_id';
    const XML_PATH_API_KEY= 'payment/tillit_payment/api_key';
    const XML_PATH_PRODUCT_TYPE= 'payment/tillit_payment/product_type';
    const XML_PATH_INVOICE_DAY= 'payment/tillit_payment/days_on_invoice';

    const XML_PATH_MODE = 'payment/tillit_payment/setting/checkout_env';
    const XML_PATH_ENABLE_COMPANY_NAME = 'payment/tillit_payment/setting/enable_company_name';
    const XML_PATH_ENABLE_COMPANY_ID= 'payment/tillit_payment/setting/enable_company_id';
    const XML_PATH_FINALIZE_PURCHASE = 'payment/tillit_payment/setting/finalize_purchase';
    const XML_PATH_ENABLE_ORDER_INTENT = 'payment/tillit_payment/setting/enable_order_intent';
    const XML_PATH_ENABLE_B2B= 'payment/tillit_payment/setting/enable_b2b_b2c_radio';
    const XML_PATH_ENABLE_REFUND= 'payment/tillit_payment/setting/initiate_payment_to_buyer_on_refund';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Payment\Model\Method\Factory $paymentMethodFactory,
        \Magento\Store\Model\App\Emulation $appEmulation,
        \Magento\Payment\Model\Config $paymentConfig,
        \Magento\Framework\App\Config\Initial $initialConfig,
        \Magento\Checkout\Model\Session $session,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Catalog\Helper\Image $cataloghelper,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\CatalogInventory\Observer\ProductQty $productQty,
        \Magento\CatalogInventory\Api\StockManagementInterface $stockManagement,
        \Magento\CatalogInventory\Model\Indexer\Stock\Processor $stockIndexerProcessor,
        \Magento\Catalog\Model\Indexer\Product\Price\Processor $priceIndexer,
        \Magento\Framework\Url $urlHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Sales\Api\OrderItemRepositoryInterface $orderItemRepository
    ) {
        parent::__construct($context,$layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);
        $this->_storeManager = $storeManagerInterface;
        $this->scopeConfig = $scopeConfig;
        $this->session = $session;
        $this->_logger = $context->getLogger();
        $this->customerSession = $customerSession;
        $this->logger = $logger;
        $this->curlClient = $curl;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->cataloghelper = $cataloghelper;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_encryptor = $encryptor;
        $this->_urlInterface = $urlInterface;
        $this->_stockManagement = $stockManagement;
        $this->_stockIndexerProcessor = $stockIndexerProcessor;
        $this->_priceIndexer = $priceIndexer;
        $this->_productQty = $productQty;
        $this->_productMetadata = $productMetadata;
        $this->_productRepository = $productRepository;
        $this->urlHelper = $urlHelper;
        $this->orderItemRepository = $orderItemRepository;

    }
     /* get order Item collection */
    public function getOrderItem($itemId)
    {
        $itemCollection = $this->orderItemRepository->get($itemId);
        return $itemCollection;
    }
    public function getProductById($id)
	{
		return $this->_productRepository->getById($id);
	}
    public function getConfig($config_path,$storecode = null)
    {
        return $this->scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$storecode
        );
    }

    public function isActive()
    {
        return $this->getConfig(self::XML_PATH_ENABLED);
    }
    public function getLogo()
    {
        return $this->getConfig(self::XML_PATH_LOGO);
    }
    public function getMerchantId()
    {
        return $this->getConfig(self::XML_PATH_MERCHANT_ID);
    }
    public function getApiKey()
    {
        return $this->_encryptor->decrypt($this->getConfig(self::XML_PATH_API_KEY));
    }
    public function getTitle()
    {
        return $this->getConfig(self::XML_PATH_TITLE);
    }
    public function getSubTitle()
    {
        return $this->getConfig(self::XML_PATH_SUBTITLE);
    }
    public function getProductType()
    {
        return $this->getConfig(self::XML_PATH_PRODUCT_TYPE);
    }
    public function getInvoiceDay()
    {
        return $this->getConfig(self::XML_PATH_INVOICE_DAY);
    }
    public function getMode()
    {
        return $this->getConfig(self::XML_PATH_MODE);
    }
    public function getEnableCompanyName()
    {
        return $this->getConfig(self::XML_PATH_ENABLE_COMPANY_NAME);
    }
    public function getEnableCompanyID()
    {
        return $this->getConfig(self::XML_PATH_ENABLE_COMPANY_ID);
    }
    public function getEnableFinalizePurchase()
    {
        return $this->getConfig(self::XML_PATH_FINALIZE_PURCHASE);
    }
    public function getEnableOrderIntent()
    {
        return $this->getConfig(self::XML_PATH_ENABLE_ORDER_INTENT);
    }
    
    public function getEnableB2B()
    {
        return $this->getConfig(self::XML_PATH_ENABLE_B2B);
    }
    public function getEnableRefund()
    {
        return $this->getConfig(self::XML_PATH_ENABLE_REFUND);
    }
    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }
    public function getStoreCode()
    {
        return $this->_storeManager->getStore()->getCode();
    }

    public function getUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getMediaUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
    }

    public function getPubUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl() . 'pub/';
    }

    public function getSearchHostUrl()
    {
        return 'https://search-api-demo-j6whfmualq-lz.a.run.app';
    }
    

    public function getCheckoutHostUrl()
    {
        $mode = $this->getMode();

        return $mode == 'prod' ? 'https://api.tillit.ai'
                                    : ($mode == 'dev' ? 'http://huynguyen.hopto.org:8084'
                                    : 'https://staging.api.tillit.ai');
    }

    public function make_request($endpoint, $payload = [], $method = 'POST')
    {
        $resultJson = $this->resultJsonFactory->create();
        try {
            if($method == "POST" || $method == "PUT")
            {
                $url = sprintf('%s%s', $this->getCheckoutHostUrl(), $endpoint);
                $params = empty($payload) ? '' :  json_encode($payload);
                $this->curlClient->addHeader("Content-Type", "application/json; charset=utf-8");
                $this->curlClient->addHeader("Tillit-Merchant-Id", $this->getMerchantId());
                $this->curlClient->setCredentials($this->getMerchantId(), $this->getApiKey());
                $this->curlClient->setOption(CURLOPT_RETURNTRANSFER, true);
                $this->curlClient->setOption(CURLOPT_TIMEOUT, 60);
                $this->curlClient->setOption(CURLOPT_SSL_VERIFYHOST, 0);
                $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
                $this->curlClient->addHeader("Content-Length", strlen($params));
                $this->curlClient->post($url, $params);
                $response = $this->curlClient->getBody();
            }else{
                $url = sprintf('%s%s', $this->getCheckoutHostUrl(), $endpoint);
                $this->curlClient->addHeader("Content-Type", "application/json; charset=utf-8");
                $this->curlClient->addHeader("Tillit-Merchant-Id", $this->getMerchantId());
                $this->curlClient->setCredentials($this->getMerchantId(), $this->getApiKey());
                $this->curlClient->setOption(CURLOPT_RETURNTRANSFER, true);
                $this->curlClient->setOption(CURLOPT_FOLLOWLOCATION, true);
                $this->curlClient->setOption(CURLOPT_TIMEOUT, 60);
                $this->curlClient->setOption(CURLOPT_SSL_VERIFYHOST, 0);
                $this->curlClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
                $this->curlClient->get($url);
                $response = $this->curlClient->getBody();
            }   
            
            return $response;
        } catch (LocalizedException $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                $e->getMessage()
            );
            $data =array('status'=>400,
            'message'=> $e->getMessage());
            $data['response']['code'] = 400;
            return json_encode($data);
        }
    }

    public static function get_tillit_error_msg($body)
    {
        if (!$body) {
            return __('Tillit empty response');
        }

        if(isset($body['response']['code']) && $body['response'] && $body['response']['code'] && $body['response']['code'] >= 400) {
            return sprintf(__('Tillit response code %d'), $body['response']['code']);
        }

        if($body) {
            if (is_string($body))
                return __($body); 
            else if (isset($body['error_details']) && is_string($body['error_details']))
                return __($body['error_details']);
            else if (isset($body['error_code']) && is_string($body['error_code']))
                return __($body['error_code']);
        }
    }



    /**
     * Compose request body for tillit create order
     *
     * @param $order
     *
     * @return bool
     */
    public function compose_tillit_order(
        $order, $order_reference, $tillit_merchant_id, $days_on_invoice, $company_id, $department, $project, $tillit_original_order_id = '')
    {
        // Get the orde taxes
        $order_taxes = $order->getFullTaxInfo();
        $tax_rate = 0;
        $billing_address = $order->getBillingAddress();
        $shipping_address = $order->getShippingAddress();
        $queryParams = ['_tillit_order_reference'=>$order_reference];
        $shipment = $order->getShipment();
        $trackNumber = '';
        $carrierName = '';
        if($shipment)
        {
            $tracksCollection = $shipment->getTracksCollection();
            foreach ($tracksCollection->getItems() as $track) {
                $trackNumber = $track->getTrackNumber();
                $carrierName = $track->getTitle();
            }
        }
        $req_body = [
            'billing_address' => [
                'city' => $billing_address->getCity(),
                'country' => $billing_address->getCountryId(),
                'organization_name' => $billing_address->getCompany(),
                'postal_code' => $billing_address->getPostcode(),
                'region' => ($billing_address->getRegion() != '') ? $billing_address->getRegion() : '',
                'street_address' => $billing_address->getStreet()[0] . (isset($billing_address->getStreet()[1]) ? $billing_address->getStreet()[1] : '')
            ],
            'buyer' => [
                'company' => [
                    'organization_number' => $company_id,
                    'country_prefix' => $billing_address->getCountryId(),
                    'company_name' => $billing_address->getCompany(),
                ],
                'representative' => [
                    'email' => $billing_address->getEmail(),
                    'first_name' => $billing_address->getFirstName(),
                    'last_name' => $billing_address->getLastName(),
                    'phone_number' => $billing_address->getTelephone()
                ],
            ],
            'buyer_department' => $department,
            'buyer_project' => $project,
            'order_note' => '',
            'line_items' => $this->get_line_items($order->getAllVisibleItems(),$order),
            'recurring' => false,
            'merchant_additional_info' => '',
            'merchant_id' => $tillit_merchant_id,
            'merchant_order_id' => strval($order->getIncrementId()),
            'merchant_reference' => '',
            'merchant_urls' => [
                'merchant_confirmation_url' => $this->urlHelper->getUrl('tillit/payment/confirm',['_tillit_order_reference' => base64_encode($order_reference)]),
                'merchant_cancel_order_url' => $this->urlHelper->getUrl('tillit/payment/cancel',['_tillit_order_reference' => base64_encode($order_reference)]),
                'merchant_edit_order_url' => '',
                'merchant_order_verification_failed_url' => $this->urlHelper->getUrl('tillit/payment/verificationfailed',['_tillit_order_reference' => base64_encode($order_reference)]),
                'merchant_invoice_url' => '',
                'merchant_shipping_document_url' => ''
            ],
            'payment' => [
                'currency' => $order->getOrderCurrencyCode(),
                'gross_amount' => strval($this->round_amt($order->getGrandTotal())),
                'net_amount' => strval($this->round_amt($order->getGrandTotal() - abs($order->getTaxAmount()))),
                'tax_amount' => strval($this->round_amt($order->getTaxAmount())),
                'tax_rate' => strval($tax_rate),
                'discount_amount' => strval($this->round_amt(abs($order->getDiscountAmount()))),
                'discount_rate' => '0',
                'type' => 'FUNDED_INVOICE',
                'payment_details' => [
                    'due_in_days' => intval($days_on_invoice),
                    'bank_account' => '',
                    'bank_account_type' => 'IBAN',
                    'payee_company_name' => '',
                    'payee_organization_number' => '',
                    'payment_reference_message' => '',
                    'payment_reference_ocr' => '',
                ]
            ],
            'shipping_address' => [
                'city' => ($shipping_address) ? $shipping_address->getCity() : '',
                'country' => ($shipping_address) ? $shipping_address->getCountryId() : '',
                'organization_name' => ($shipping_address) ? $shipping_address->getCompany() : '',
                'postal_code' =>($shipping_address) ? $shipping_address->getPostcode() : '',
                'region' => ($shipping_address && $shipping_address->getRegion() != '') ? $shipping_address->getRegion() : '',
                'street_address' => ($shipping_address) ?  $shipping_address->getStreet()[0] . (isset($shipping_address->getStreet()[1]) ? $shipping_address->getStreet()[1] : '') : ''
            ],
            'shipping_details' => [
                'carrier_name' => $carrierName,
                'tracking_number' => $trackNumber,
                // 'carrier_tracking_url' => '',
                'expected_delivery_date' => date('Y-m-d', strtotime('+ 7 days'))
            ]
        ];

        if ($tillit_original_order_id) {
            $req_body['original_order_id'] = $tillit_original_order_id;
        }
        return $req_body;
    }

    public function round_amt($amt)
    {
        return number_format($amt, 2, '.', '');
    }

    /**
     * Format the cart items
     *
     * @return array
     */
    public function get_line_items($line_items, $order)
    {

        $items = [];

        
            foreach($line_items as $item) {
                if($childProd = current($item->getChildrenItems())){
                    $productImage = $this->cataloghelper->init($childProd->getProduct(), 'small_image')->setImageFile($childProd->getProduct()->getSmallImage())->resize(200, 200)->getUrl();
                }
                else{
                    $productImage = $this->cataloghelper->init($item->getProduct(), 'small_image')->setImageFile($item->getProduct()->getSmallImage())->resize(200, 200)->getUrl();
                }
              

            $product = [
                'name' => $item->getName(),
                'description' => substr($item->getDescription(), 0, 255),
                'gross_amount' => strval($this->round_amt($item->getRowTotal() - abs($item->getDiscountAmount()) + $item->getTaxAmount())),
                'net_amount' =>  strval($this->round_amt($item->getRowTotal() - abs($item->getDiscountAmount()))),
                'discount_amount' => strval($this->round_amt(abs($item->getDiscountAmount()))),
                'tax_amount' => strval($this->round_amt($item->getTaxAmount())),
                'tax_class_name' => 'VAT ' . $this->round_amt($item->getTaxPercent()) . '%',
                'tax_rate' => strval($this->round_amt($item->getTaxPercent())),
                'unit_price' => strval($this->round_amt($item->getPrice())),
                'quantity' => $item->getQtyOrdered(),
                'quantity_unit' => 'item',
                'image_url' => $productImage ? $productImage : '',
                'product_page_url' => $item->getProduct()->getProductUrl(),
                'type' => 'PHYSICAL',
                'details' => [
                    'barcodes' => [
                        [
                            'type' => 'SKU',
                            'id' =>$item->getSku(),
                        ]
                    ]
                ]
            ];

            $categoryIds = $item->getProduct()->getCategoryIds();

            $categories = $this->getCategoryCollection()
                    ->addAttributeToFilter('entity_id', $categoryIds);
            $product['details']['categories'] = [];

            foreach($categories as $category) {
                $product['details']['categories'][] = $category->getName();
            }

            $items[] = $product;

        }

        if ($order->getShippingAmount() == 0) {
        }else{

            $tax_rate = 1.0 * $order->getShippingTaxAmount() / $order->getShippingAmount();
            $shipping_line = [
                'name' => 'Shipping - ' . $order->getShippingMethod(),
                'description' => '',
                'gross_amount' => strval($this->round_amt($order->getShippingAmount())),
                'net_amount' =>  strval($this->round_amt($order->getShippingAmount() - $order->getShippingTaxAmount())),
                'discount_amount' => '0',
                'tax_amount' => strval($this->round_amt($order->getShippingTaxAmount())),
                'tax_class_name' => 'VAT ' . $this->round_amt($tax_rate * 100) . '%',
                'tax_rate' => strval($tax_rate),
                'unit_price' => strval($this->round_amt($order->getShippingAmount())),
                'quantity' => 1,
                'quantity_unit' => 'sc', // shipment charge
                'image_url' => '',
                'product_page_url' => '',
                'type' => 'SHIPPING_FEE'
            ];

            $items[] = $shipping_line;
        }

        return $items;

    }

    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');        
        
        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }
                
        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }
        
        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }
        
        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize); 
        }    
        
        return $collection;
    }
    protected function OrderComment($order,$message = '')
    {       
        if ($message != '')
        {
            $message = $order->addStatusHistoryComment($message);
            $message->setIsCustomerNotified(null);
        }
		
        return $message;
    }
    public function ProcessOrder($order) {
        $payment = $order->getPayment();
        $payment->setTransactionId($order->getTillitOrderId())       
        ->setPreparedMessage($this->OrderComment("TILLIT_PAYMENT :: Payment has been verified."))
        ->setShouldCloseParentTransaction(true)
        ->setIsTransactionClosed(1)
        ->registerCaptureNotification($order->getGrandTotal(),true);

        $payment->save();
        $order->setState($order::STATE_PROCESSING);
        $order->setStatus($order::STATE_PROCESSING);
        $order->save();

        $invoice = $payment->getCreatedInvoice();
        if ($invoice && !$order->getEmailSent()) {
            //$this->_orderSender->send($this->_order);
            $order->addStatusHistoryComment(
                __('You notified customer about invoice #%1.', $invoice->getIncrementId())
            )->setIsCustomerNotified(
                true
            )->save();
        }
    }
    public function getUrls($route, $params = [])
    {
        return $this->_getUrl($route, $params);
    }

    public function orderFailed($order, $reason) {
        if ($order->getState() != Tillit::STATUS_FAILED) {
            $order->setStatus(Tillit::STATUS_FAILED);
            $order->setState(Tillit::STATE_FAILED);
            $order->save();
            $order->addStatusToHistory(Tillit::STATUS_FAILED, $reason, false );
            $order->save();
            return true;
        }
        return false;
    }
    /**
     * Restores quote
     *
     * @return bool
     */
    public function restoreQuote()
    {
        $result = $this->session->restoreQuote();
        // Versions 2.2.4 onwards need an explicit action to return items.
        if ($result && $this->isReturnItemsToInventoryRequired()) {
            $this->returnItemsToInventory();
        }

        return $result;
    }

    /**
     * Checks if version requires restore quote fix.
     *
     * @return bool
     */
    private function isReturnItemsToInventoryRequired()
    {
        $version = $this->getMagentoVersion();
        return version_compare($version, "2.2.4", ">=");
    }
    public function getMagentoVersion() {
        return $this->_productMetadata->getVersion();
    }
    /**
     * Returns items to inventory.
     *
     */
    private function returnItemsToInventory()
    {
        // Code from \Magento\CatalogInventory\Observer\RevertQuoteInventoryObserver
        $quote = $this->session->getQuote();
        $items = $this->_productQty->getProductQty($quote->getAllItems());
        $revertedItems = $this->_stockManagement->revertProductsSale($items, $quote->getStore()->getWebsiteId());

        // If the Magento 2 server has multi source inventory enabled, 
        // the revertProductsSale method is intercepted with new logic that returns a boolean.
        // In such case, no further action is necessary.
        if (gettype($revertedItems) === "boolean") {
            return;
        }

        $productIds = array_keys($revertedItems);
        if (!empty($productIds)) {
            $this->_stockIndexerProcessor->reindexList($productIds);
            $this->_priceIndexer->reindexList($productIds);
        }
        // Clear flag, so if order placement retried again with success - it will be processed
        $quote->setInventoryProcessed(false);
    }

    public function printLog($log)
    {
        $writer = new Stream(BP . '/var/log/Tillit.log');
        $logger = new Logger();
        $logger->addWriter($writer);
        $logger->info($log);
    }

    /**
     * Recursively compare arrays
     *
     * @param $src_arr
     * @param $dst_arr
     *
     * @return array
     */
    public function array_diff_r($src_arr, $dst_arr) {
        $diff = array();

        foreach ($src_arr as $key => $val) {
            if (array_key_exists($key, $dst_arr)) {
                if (is_array($val)) {
                    $sub_diff = $this->array_diff_r($val, $dst_arr[$key]);
                    if (count($sub_diff)) {
                        $diff[$key] = $sub_diff;
                    }
                } else {
                    if ($val != $dst_arr[$key]) {
                        $diff[$key] = $val;
                    }
                }
            } else {
                $diff[$key] = $val;
            }
        }
        return $diff;
    }


     /**
     * Get tillit meta from DB and Tillit server
     *
     * @param $order
     */
    public function get_save_tillit_meta($order)
    {

        $tillit_order_id = $order->getTillitOrderId();
        if (!$tillit_order_id) {
            return;
        }
        $payment = $order->getPayment();
        $original_orders_data = $payment->getAdditionalInformation();
        $tillit_original_order_id = $tillit_order_id;

        $order_reference = $order->getTillitOrderReference();
        if (isset($original_orders_data['gateway_data']['tillit_merchant_id'])) {
            $tillit_merchant_id = $original_orders_data['gateway_data']['tillit_merchant_id'];
        }else
        {
            $tillit_merchant_id = $this->getMerchantId();
        }
        $days_on_invoice = (isset($original_orders_data['gateway_data']['days_on_invoice'])) ? $original_orders_data['gateway_data']['days_on_invoice'] : $this->getInvoiceDay();

        $company_id = $order->getCompanyId();
        if ($company_id) {
            $department = $order->getDepartment();
            $project = $order->getProject();
        } else {
            $response = $this->make_request("/v1/order/${tillit_order_id}", [], 'GET');

            $body = json_decode($response, true);
            if (!$body || !$body['buyer'] || !$body['buyer']['company'] || !$body['buyer']['company']['organization_number']) {
                $order->addStatusHistoryComment(sprintf(__('Missing company ID, please check with Tillit admin for id %s'), $tillit_order_id));
                return;
            }
            $company_id = $body['buyer']['company']['organization_number'];
            $department = $body['buyer_department'];
            $project = $body['buyer_project'];
            $order->setCompanyId($company_id);
            $order->setDepartment($department);
            $order->setProject($project);
            $order->save();
        }

        return array(
            'order_reference' => $order_reference,
            'tillit_merchant_id' => $tillit_merchant_id,
            'days_on_invoice' => $days_on_invoice,
            'company_id' => $company_id,
            'department' => $department,
            'project' => $project,
            'tillit_order_id' => $tillit_order_id,
            'tillit_original_order_id' => $tillit_original_order_id,
        );


    }
    /**
     * Run the update execution
     *
     * @param $order
     */
    public function update_tillit_order($order)
    {

        $tillit_order_id = $order->getTillitOrderId();


        // 1. Get information from the current order
        $tillit_meta = $this->get_save_tillit_meta($order);
        if (!$tillit_meta) return;

        // 3. Create new order
        $payload =  $this->compose_tillit_order(
            $order,
            $tillit_meta['order_reference'],
            $tillit_meta['tillit_merchant_id'],
            $tillit_meta['days_on_invoice'],
            $tillit_meta['company_id'],
            $tillit_meta['department'],
            $tillit_meta['project'],
            $tillit_meta['tillit_original_order_id']);
        $response = $this->make_request('/v1/order', $payload,'PUT');

        $response = json_decode($response, true);

        if(!isset($response)) {
            $order->addStatusHistoryComment(__('Could not recreate new Tillit order'))->save();
            return;
        }

        $tillit_err = $this->get_tillit_error_msg($response);
        if ($tillit_err) {
            $order->addStatusHistoryComment(__('Could not recreate new Tillit order, please check with Tillit admin'))->save();
            return;
        }

        $new_tillit_order_id = $response['original_order_id'];
        
        unset($payload['original_order_id']);
        $payload['gateway_data']['original_order_id'] = $response['original_order_id'];
        $payload['gateway_data']['tillit_merchant_id'] = $this->getMerchantId();
        $payload['gateway_data']['days_on_invoice'] =  $this->getInvoiceDay();
        $payload['gateway_data']['order_reference'] = $tillit_meta['order_reference'];
        $payment = $order->getPayment();
        $payment->setAdditionalInformation($payload);
        $payment->save();

        $order->setTillitOrderId($response['original_order_id']);
        $order->save();

        $order->addStatusHistoryComment(sprintf(__('Order updated with tillit id %s'),$response['original_order_id']))->save();
    }

    public function compose_tillit_refund($credit_memo, $amount, $currency, $initiate_payment_to_buyer,$order)
    {

        $req_body = [
            'amount' => strval($this->round_amt($amount)),
            'currency' => $currency,
            'initiate_payment_to_buyer' => $initiate_payment_to_buyer,
            'line_items' => $this->get_refund_line_items($credit_memo->getItems(), $credit_memo,$order)
        ];

        return $req_body;
    }
    public function get_refund_line_items($line_items, $credit_memo,$order)
    {

            $items = [];
            foreach($line_items as $item) {
            $item_data = $item->getData();
            if(isset($item_data['row_total'])) {
                $product = $this->getProductById($item->getProductId());
                $productImage = $product->getData('small_image');
                $orderItem = $this->getOrderItem($item->getOrderItemId());
                $products = [
                    'name' => $product->getName(),
                    'description' => substr($product->getDescription(), 0, 255),
                    'gross_amount' => strval($this->round_amt($item->getRowTotal() - abs($item->getDiscountAmount()) + $item->getTaxAmount())),
                    'net_amount' =>  strval($this->round_amt($item->getRowTotal() - abs($item->getDiscountAmount()))),
                    'discount_amount' => strval($this->round_amt(abs($item->getDiscountAmount()))),
                    'tax_amount' => strval($this->round_amt($item->getTaxAmount())),
                    'tax_class_name' => 'VAT ' . $this->round_amt($orderItem->getTaxPercent()) . '%',
                    'tax_rate' => strval($this->round_amt($orderItem->getTaxPercent())),
                    'unit_price' => strval($this->round_amt($item->getPrice())),
                    'quantity' => $item->getQty(),
                    'quantity_unit' => 'item',
                    'image_url' => $productImage ? $productImage : '',
                    'product_page_url' => $product->getProductUrl(),
                    'type' => 'PHYSICAL',
                    'details' => [
                        'barcodes' => [
                            [
                                'type' => 'SKU',
                                'id' =>$item->getSku(),
                            ]
                        ]
                    ]
                ];

                $categoryIds = $product->getCategoryIds();

                $categories = $this->getCategoryCollection()
                        ->addAttributeToFilter('entity_id', $categoryIds);
                $products['details']['categories'] = [];

                foreach($categories as $category) {
                    $products['details']['categories'][] = $category->getName();
                }

                $items[] = $products;
            }
            

        }

        if ($credit_memo->getShippingAmount() == 0) {
        }else{

            $tax_rate = 1.0 * $credit_memo->getShippingTaxAmount() / $credit_memo->getShippingAmount();
            $shipping_line = [
                'name' => 'Shipping - ' . $order->getShippingMethod(),
                'description' => '',
                'gross_amount' => strval($this->round_amt($credit_memo->getShippingAmount())),
                'net_amount' =>  strval($this->round_amt($credit_memo->getShippingAmount() - $credit_memo->getShippingTaxAmount())),
                'discount_amount' => '0',
                'tax_amount' => strval($this->round_amt($credit_memo->getShippingTaxAmount())),
                'tax_class_name' => 'VAT ' . $this->round_amt($tax_rate * 100) . '%',
                'tax_rate' => strval($tax_rate),
                'unit_price' => strval($this->round_amt($credit_memo->getShippingAmount())),
                'quantity' => 1,
                'quantity_unit' => 'sc', // shipment charge
                'image_url' => '',
                'product_page_url' => '',
                'type' => 'SHIPPING_FEE'
            ];

            $items[] = $shipping_line;
        }

        return $items;

    }
}

