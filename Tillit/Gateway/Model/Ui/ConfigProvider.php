<?php


namespace Tillit\Gateway\Model\Ui;

use Magento\Framework\UrlInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\ProductMetadataInterface;
use \Magento\Framework\View\Asset\Repository;
use Tillit\Gateway\Helper\Config;

class ConfigProvider implements ConfigProviderInterface
{
        
    /**
     * Injected config object
     *
     * @var \Magento\Payment\Gateway\ConfigInterface
     */
    protected $_assetRepo;

    
    /**
     * Injected url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * Product metadata object
     *
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * Inject all needed object for getting data from config
     *
     * @param ConfigInterface          $config
     * @param UrlInterface             $urlInterface
     * @param CheckoutSession          $checkoutSession
     * @param ProductMetadataInterface $productMetadata
     */
    public function __construct(
        UrlInterface $urlInterface,
        CheckoutSession $checkoutSession,
        ProductMetadataInterface $productMetadata,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        Config $helper 
    ) {
        
        $this->urlBuilder = $urlInterface;
        $this->checkoutSession = $checkoutSession;
        $this->productMetadata = $productMetadata;
        $this->_assetRepo = $assetRepo;
        $this->helper = $helper;
    }

    /**
     * Retrieve assoc array of checkout configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return array(
            'payment' => array(
                Config::CODE => array(
                    'payment_code'=> self::CODE,
                    'messages' => [
                        'subtitle_order_intent_ok' =>$this->helper->getSubTitle(),
                        'subtitle_order_intent_reject' => __('EHF Invoice is not available for this order'),
                        'amount_min' => sprintf(__('Minimum Payment using Tillit is %s NOK'), '200'),
                        'amount_max' => sprintf(__('Maximum Payment using Tillit is %s NOK'), '250,000'),
                    ],
                    'tillit_search_host' => $this->helper->getSearchHostUrl(),
                    'tillit_checkout_host' => $this->helper->getCheckoutHostUrl(),
                    'company_name_search' => $this->helper->getEnableCompanyName(),
                    'company_id_search' => $this->helper->getEnableCompanyID(),
                    'enable_order_intent' => $this->helper->getEnableOrderIntent(),
                    'merchant_id' => $this->helper->getMerchantId()
                )
            )
        );
    }
}
