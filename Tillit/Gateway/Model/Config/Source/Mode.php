<?php

namespace Tillit\Gateway\Model\Config\Source;

class Mode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => '', 'label'=>__('Select Mode')],
                ['value' => 'prod', 'label'=>__('Production')],
                ['value' => 'stg', 'label'=>__('Staging')],
                ['value' => 'dev', 'label'=>__('Development')]];
    }
}