<?php

namespace Tillit\Gateway\Model\Config\Source;

class Product implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => '', 'label'=>__('Select Product')],
                ['value' => 'product_funded', 'label'=>__('Funded invoice')],
                ['value' => 'product_merchant', 'label'=>__('Merchant Invoice (coming soon)'),'disabled'=>"disabled"],
                ['value' => 'product_administered', 'label'=>__('Administered invoice (coming soon)'),'disabled'=>"disabled"]];
    }
}