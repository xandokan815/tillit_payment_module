<?php

namespace Tillit\Gateway\Model;
use Magento\Framework\App\RequestInterface;

class Tillit extends \Magento\Payment\Model\Method\AbstractMethod
{
    const CODE = 'tillit_payment';

    const STATUS_NEW    = 'tillit_new';
    const STATUS_FAILED = 'tillit_failed';
    const STATE_FAILED = 'canceled';
    
    protected $_code = self::CODE;

    protected $_countryFactory;

    protected $_minAmount = null;
    protected $_maxAmount = null;
    protected $_canAuthorize                = true;
    protected $_canCapture                  = true;
    protected $_canRefund                   = true;
    protected $_canRefundInvoicePartial     = true;

    protected $_isInitializeNeeded = true;


    protected $_helper;
    
    /**
     * @var PriceCurrencyInterface
     */
    //protected $priceCurrency;
    
    public function __construct(
        RequestInterface $request,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Tillit\Gateway\Helper\Config $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        \Tillit\Gateway\Helper\UrlCookie $UrlCookie,
        array $data = []
    ) {
        $this->request = $request;
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $helper,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
        $this->_helper = $helper;
        $this->UrlCookie = $UrlCookie;
        //$this->_helper->setMethodCode($this->_code);

    }

    /**
     * Instantiate state and set it to state object.
     *
     * @param string                        $paymentAction
     * @param \Magento\Framework\DataObject $stateObject
     */
    public function initialize($paymentAction, $stateObject)
    {
        $payment = $this->getInfoInstance();
        $order = $payment->getOrder();
        $order->setCanSendNewEmailFlag(false);		
        $billing_address = $order->getBillingAddress();
        $stateObject->setState(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);
        $stateObject->setStatus('pending_payment');
        $stateObject->setIsNotified(false);

        $this->UrlCookie->delete();
        
        $order_reference = rand();
        $order->setTillitOrderReference($order_reference);
        $order->save();

        $payload = $this->_helper->compose_tillit_order(
            $order,
            $order_reference,
            $this->_helper->getMerchantId(),
            $this->_helper->getInvoiceDay(),
            $billing_address->getCompanyId(),
            $billing_address->getDepartment(),
            $billing_address->getProject()
        );
        // Create order
        $response = $this->_helper->make_request('/v1/order', $payload );

        $response = json_decode($response, true);

        if(!isset($response)) {
            throw new  \Magento\Framework\Exception\LocalizedException(__('Something went wrong.'));
        }
        if(isset($response['result']) && $response['result'] === 'failure') {
            throw new  \Magento\Framework\Exception\LocalizedException($response);
        }

        // If we have an error
        if(isset($response['response']['code']) && ($response['response']['code'] === 401 || $response['response']['code'] === 403)) {
            throw new  \Magento\Framework\Exception\LocalizedException(__('Website is not properly configured with Tillit payment'));
        }

        // If we have an error
        if(isset($response['response']['code']) &&  $response['response']['code'] === 400) {
            throw new  \Magento\Framework\Exception\LocalizedException(__('Something went wrong.'));
        }
        

        $tillit_err = $this->_helper->get_tillit_error_msg($response);
        if ($tillit_err) {
            throw new  \Magento\Framework\Exception\LocalizedException($tillit_err);
        }
        if(isset($response['response']['code']) &&  $response['response']['code'] >= 400) {
            throw new  \Magento\Framework\Exception\LocalizedException(__('EHF Invoice is not available for this order.'));
        }
        $order->setTillitOrderId($response['id']);
        $order->save();
        $payload['gateway_data']['original_order_id'] = $response['id'];
        $payload['gateway_data']['tillit_merchant_id'] = $this->_helper->getMerchantId();
        $payload['gateway_data']['days_on_invoice'] =  $this->_helper->getInvoiceDay();
        $payload['gateway_data']['order_reference'] = $order_reference;
        $payment->setAdditionalInformation($payload);
        $payment->save();
        $this->UrlCookie->set($response['tillit_urls']['verify_order_url']);
        return $this;
    }
    
    public function setMethodCode($code) {
        $this->_code = $code;
    }
    
    public function getMethodCode() {
        return $this->_code;
    }
    /**
     * Determine method availability based on quote amount and config data
     *
     * @param \Magento\Quote\Api\Data\CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        if (!$this->_helper->getMerchantId() || !$this->_helper->isActive() || $this->_helper->getApiKey() == '') {
            return false;
        }
        return parent::isAvailable($quote);
    }


    /**
     * Get instructions text from config
     *
     * @return string
     */
    public function getInstructions()
    {
        return __('You will be redirected to the Tillit website when you place an order.');
    }
    

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    { 
        if (!$this->canAuthorize()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The authorize action is not available.'));
        }
        return $this;
    }
    public function capture(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        if (!$this->canAuthorize()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The authorize action is not available.'));
        }
        return $this;
    }

    public function refund(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        if (!$this->canRefund()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The refund action is not available.'));
        }
        return $this;
    }
}