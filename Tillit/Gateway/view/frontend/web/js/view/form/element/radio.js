define([
    'Magento_Ui/js/form/element/abstract',
    'uiRegistry',
    'jquery',
    'Tillit_Gateway/js/tillit',
], function(Abstract,registry, $,Tillit) {
    'use strict';

    return Abstract.extend({
        defaults: {
            checked: false,
        },
        initialize: function () {
            this._super();
            return this;
        },

        /**
         * Handle radio click, return true to check te radio
         */
        click: function(data, event) {
            
            this.change(event.target.value);

            return true;
        },

        /**
         * Change value of radio
         */
        change: function(value) {
            
            if (value === 'business') {
                $('#radio-business').prop("checked", true);
                this.toggleFields(value);
            } else if (value === 'personal') {
                $('#radio-personal').prop("checked", true);
                this.toggleFields(value);
            }
            this.value(value);
        },

        toggleFields : function (value)
        {
            
            if (value === 'business') {

                registry.get(function(component){
                    if(component.parentName != "undefined" && component.parentName !=  undefined && component.parentName !=  "" && component.parentName.indexOf("checkout.steps.billing-step.payment.payments-list") != -1) 
                    {   
                        if(component.inputName == "custom_attributes[company_id]")
                        {
                            component.visible(true).required(true);
                        }
                        if(component.inputName == "custom_attributes[company_name]" && window.company_name_search == '1')
                        {
                            component.visible(true).required(true);
                        }
                        if(component.inputName == "custom_attributes[department]" || component.inputName == "custom_attributes[project]")
                        {
                            component.visible(true).required(false);
                        }
                    }
                });

                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company_id").visible(true).required(true);
                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company_name").visible(true).required(true);
                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.department").visible(true).required(false);
                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.project").visible(true).required(false);


                if(Tillit.gettillitApproved()) {
                    Tillit.settillitMethodHidden(false);
                }
                $(".tillit-payment-method .checkout").attr('disabled', false);
            } else if (value === 'personal') {
                
                registry.get(function(component){
                    
                    if(component.parentName != "undefined" && component.parentName !=  undefined && component.parentName !=  "" && component.parentName.indexOf("checkout.steps.billing-step.payment.payments-list") != -1) 
                    {   
                        
                        if(component.inputName == "custom_attributes[company_id]" || component.inputName == "custom_attributes[department]" || component.inputName == "custom_attributes[project]")
                        {
                            component.visible(false).required(false);
                        }
                        if(component.inputName == "custom_attributes[company_name]" && window.company_name_search == '1')
                        {
                            component.visible(false).required(false);
                        }
                    }
                });


                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company_id").visible(false).required(false);
                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company_name").visible(false).required(false);
                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.department").visible(false).required(false);
                registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.project").visible(false).required(false);

                const tillitPaymentMethod = $('input[value="tillit_payment"]');
                tillitPaymentMethod.attr('disabled', true);
                $(".tillit-payment-method .checkout").attr('disabled', true);

                Tillit.settillitMethodHidden(true);
            }
        
             // Show or hide the payment method
            if(this.name.indexOf('billing-step') != -1)
            {
                Tillit.toggleMethod(false)
            }
            else{
                Tillit.toggleMethod();
            }
        }
    });
});