define([
    'Magento_Ui/js/form/element/abstract',
    'uiRegistry',
    'jquery',
    'Tillit_Gateway/js/tillit',
], function(Abstract,registry, $,Tillit) {
    'use strict';

    return Abstract.extend({
        inits : function(){
            $("#co-shipping-form").on('change',"[name='country_id']",function(){
                
                Tillit.setCompanyValue('country_prefix',this.value);
                Tillit.ApproveOrder();
            });
            $("#co-shipping-form").on('blur',"[name='firstname']",function(){
                
                Tillit.setRepresentativeValue('first_name',this.value);
                Tillit.ApproveOrder();
            });
            $("#co-shipping-form").on('blur',"[name='lastname']",function(){
                
                Tillit.setRepresentativeValue('last_name',this.value);
                Tillit.ApproveOrder();
            });
            $("#co-shipping-form").on('blur',"[name='telephone']",function(){
            
                Tillit.setRepresentativeValue('phone_number',this.value);
                Tillit.ApproveOrder();
            });
            $(document).on('blur',"[name='username']",function(){
            
                Tillit.setRepresentativeValue('email',this.value);
                Tillit.ApproveOrder();
            });
            $("#co-shipping-form").on('blur',"[name='custom_attributes[company_id]']",function(){
            
                Tillit.setCompanyValue('organization_number',this.value);
                Tillit.ApproveOrder();
            }); 
        },
        populateFields  : function() {
            this.inits();
            Tillit.populateFields();
            Tillit.ApproveOrder();
        },

    });
});