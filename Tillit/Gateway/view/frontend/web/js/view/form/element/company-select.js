/**
 * @api
 */
define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/select',
    'ko',
    'jquery',
    'Tillit_Gateway/js/select2.min',
    'Tillit_Gateway/js/tillit',
], function (_, registry, Abstract, ko, $,select2,Tillit) {
    'use strict';
    let tillitSearchCache
    const tillitSearchLimit = 50
    ko.bindingHandlers.select2 = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext){
            var $element = $(element);
            var options = ko.unwrap(valueAccessor());
            

            $element.select2({
                minimumInputLength: 3,
                width: '100%',
                escapeMarkup: function(markup) {
                    return markup
                },
                templateResult: function(data)
                {
                    return data.html
                },
                templateSelection: function(data) {
                    return data.text
                },
                ajax: {
                    dataType: 'json',
                    delay: 200,
                    url: function(params){
                        params.page = params.page || 1
                        return window.tillit_search_host + '/search?limit=' + tillitSearchLimit + '&offset=' + ((params.page - 1) * tillitSearchLimit) + '&q=' + params.term
                    },
                    data: function()
                    {
                        return {}
                    },
                    processResults: function(response, params)
                    {

                        tillitSearchCache = response

                        if(response.status !== 'success') 

                        var items = [];
                        

                        if(response.status !== 'success') {
                            
                        }else
                        {
                            var items = [];
                            for(let i = 0; i < response.data.items.length; i++) {

                                var item = response.data.items[i]
                                items.push({
                                    id: item.name,
                                    text: item.name,
                                    html: item.highlight + ' (' + item.id + ')',
                                    company_id: item.id,
                                    approved: false
                                })
    
                            }
                        }
                        return {
                            results: items,
                            pagination: {
                                more: (params.page * tillitSearchLimit) < response.data.total
                            }
                        }

                    }
                }
            }).on('select2:select', function(e){

                // Get the option data
                const data = e.params.data
                
                var parent = $element.parent().parent();
                var is_shipping_company = true;
                if(parent.attr('name').indexOf('billingAddress') != -1)
                {
                    is_shipping_company = false;
                }
                if(window.company_id_search && window.company_id_search === '1') {
                    
                    // Set the company ID
                    Tillit.setCompanyValue('organization_number',data.company_id); 

                    var shipping_company_id  = registry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company_id');
                    var billing_company_id  = registry.get('checkout.steps.billing-step.payment.payments-list.tillit_payment-form.form-fields.company_id');
                    var shipping_company  = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company");
                    var billing_company  = registry.get("checkout.steps.billing-step.payment.payments-list.tillit_payment-form.form-fields.company");
                    // Set the company ID
                    
                    if(is_shipping_company)
                    {
                        if(shipping_company_id)
                        {
                            shipping_company_id.value(data.company_id);
                        }
                        if(shipping_company)
                        {
                            shipping_company.value(data.text);
                        }
                    }
                    else{

                        registry.get(function(component){
                    
                            if(component.parentName != "undefined" && component.parentName !=  undefined && component.parentName !=  "" && component.parentName.indexOf("checkout.steps.billing-step.payment.payments-list") != -1) 
                            {   
        
                                if(component.inputName == "custom_attributes[company_id]")
                                {
                                    component.value(data.company_id);
                                }
                                if(component.inputName == "company")
                                {
                                    component.value(data.text);
                                }
                            }
                        });
                    }
                }

                // Set the company name
                Tillit.setCompanyValue('company_name',data.id);
                // Get the company approval status
                Tillit.ApproveOrder(is_shipping_company)

                // Fetch the company data
                const addressResponse = jQuery.ajax({
                    dataType: 'json',
                    url: window.tillit_checkout_host + '/v1/company/' + data.company_id + '/address'
                })

                addressResponse.done(function(response){

                    var parent = $element.parent().parent();
                    var is_shipping_company = true;
                    if(parent.attr('name').indexOf('billingAddresstillit_payment') != -1)
                    {
                        is_shipping_company = false;
                    }
                        // If we have the company location
                    if(response.company_location) {

                        // Get the company location object
                        const companyLocation = response.company_location;
                        
                        var street  = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.street.0");
                        var city  = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.city");
                        var postcode  = registry.get("checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.postcode");

                        var billing_street  = registry.get("checkout.steps.billing-step.payment.payments-list.tillit_payment-form.form-fields.street.0");
                        var billing_city  = registry.get("checkout.steps.billing-step.payment.payments-list.tillit_payment-form.form-fields.city");
                        var billing_postcode  = registry.get("checkout.steps.billing-step.payment.payments-list.tillit_payment-form.form-fields.postcode");
                        if(is_shipping_company)
                        {
                            if(street)
                            {
                                street.value(companyLocation.street_address)
                            }
                            if(city)
                            {
                                city.value(companyLocation.municipality_name)
                            }
                            if(postcode)
                            {
                                postcode.value(companyLocation.postal_code)
                            }
                        }else
                        {
                            registry.get(function(component){
                    
                                if(component.parentName != "undefined" && component.parentName !=  undefined && component.parentName !=  "" && component.parentName.indexOf("checkout.steps.billing-step.payment.payments-list") != -1) 
                                {   
            
                                    if(component.inputName == "street.0")
                                    {
                                        component.value(companyLocation.street_address);
                                    }
                                    if(component.inputName == "city")
                                    {
                                        component.value(companyLocation.municipality_name)
                                    }
                                    if(component.inputName == "postcode")
                                    {
                                        component.value(companyLocation.postal_code)
                                    }
                                }
                            });
                        }
                    }

                })

            })


        }
    }

    return Abstract.extend({
        defaults: {
            select2: {},
            visible: false,
        },
        populateFields  : function() {
            Tillit.populateFields();
        },

        initialize: function () {
            this._super();

            
            return this;
        },
        initObservable: function () {
            this._super();
            this.observe('select2');
            return this;
        },

        normalizeData: function (value) {

            this.getCurrentValue(value);

            return value;
        },
        getCurrentValue: function(value){

            if(value) {
                var self = this;

                /* $.post(this.select2().ajax.url, { id: value, form_key: window.FORM_KEY},function (data) {
                    self.addCurrentValueToOptions(data.items, value);
                }); */
            }
        },
        addCurrentValueToOptions: function(items,value){

            var self = this;

            var options = [];
            
            $.each(items, function(key,item) {
                options.push({'label': item.text, 'labeltitle': item.text, 'value': item.id});
            });

            this.setOptions(options);

            if(value) {
                this.value(value);
            }

        },
        change: function(att, event){

            var $element = $(event.target);

            if(this.select2()) {

                var items = [];
                var values = $element.val();
                if(values) {
                    var label = $element.find("option[value='"+values+"']").text();
                    items.push({'text':label,'id':values})
                }

                this.addCurrentValueToOptions(items,false);

            }

        },
        getPreview: function () {
            var value = this.value(),
                option = this.indexedOptions[value],
                preview = option ? option.label : '';

            this.preview(preview);

            return preview;
        },
    });
});
