define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component,
              rendererList) {
        'use strict';
        rendererList.push(
            {
                type: 'tillit_payment',
                component: 'Tillit_Gateway/js/view/payment/method-renderer/tillit-method'
            }
        );
        return Component.extend({});
    }
);