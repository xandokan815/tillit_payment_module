define([
    'ko',
    'jquery',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/action/set-payment-information',
    'Magento_Checkout/js/action/place-order',
    ],
    function (ko, $, Component, quote, fullScreenLoader, setPaymentInformationAction, placeOrder) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Tillit_Gateway/payment/view'
            },

            context: function() {
                return this;
            },

            getCode: function() {
                return 'tillit_payment';
            },
            getCheckoutLogo: function() {
                return window.payment_logo;
            },
            getData: function () {
                var data = {
                    'method': this.getCode(),
                    'additional_data': {
                    }
                };

                return data;
            },
            isActive: function() {
                return true;
            },
            redirectAfterPlaceOrder: false,
            
            afterPlaceOrder : function() {
                var Url = $.cookie('tillit_url');
                if(Url != '' && Url != null && Url != 'null')
                {
                    $.mage.redirect($.cookie('tillit_url'));
                    $.cookie("tillit_url", null);
                }
                else{
                   /*  $.mage.redirect(window.cancelOrderUrl); */
                }
            },
            
        });
    }
);