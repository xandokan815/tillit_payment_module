define([
    'mage/utils/wrapper',
    'Tillit_Gateway/js/tillit'
    ], function(wrapper,Tillit) {
    'use strict';
    
    return function (selectShippingAddressAction) {
        return wrapper.wrap(selectShippingAddressAction, function(originalAction) {
            
            originalAction();
            Tillit.populateFields();
            Tillit.ApproveOrder();
            return ;
        });
    };
    });
    