define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Tillit_Gateway/js/tillit'
], function ($, wrapper, quote,Tillit) {
    'use strict';

    return function (setBillingAddressAction) {
        return wrapper.wrap(setBillingAddressAction, function (originalAction, messageContainer) {

            /* Tillit.populateFields();
            Tillit.ApproveOrder(false); */
            var billingAddress = quote.billingAddress();

            if (billingAddress != undefined) {

                if (billingAddress['extension_attributes'] === undefined) {
                    billingAddress['extension_attributes'] = {};
                }
                if (billingAddress.customAttributes['account_type']) {
                    if ($.isPlainObject(billingAddress.customAttributes['account_type'])) {
                        billingAddress['extension_attributes']['account_type'] = billingAddress.customAttributes['account_type'].value;
                    } else {
                        billingAddress['extension_attributes']['account_type'] = billingAddress.customAttributes['account_type'];
                    }
                    //shippingAddress['extension_attributes']['account_type'] = shippingAddress.customAttributes['account_type'].value;
                }

                if (billingAddress.customAttributes['company_id']) {
                    if ($.isPlainObject(billingAddress.customAttributes['company_id'])) {
                        billingAddress['extension_attributes']['company_id'] = billingAddress.customAttributes['company_id'].value;
                    } else {
                        billingAddress['extension_attributes']['company_id'] = billingAddress.customAttributes['company_id'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }

                if (billingAddress.customAttributes['department']) {
                    if ($.isPlainObject(billingAddress.customAttributes['department'])) {
                        billingAddress['extension_attributes']['department'] = billingAddress.customAttributes['department'].value;
                    } else {
                        billingAddress['extension_attributes']['department'] = billingAddress.customAttributes['department'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }
                if (billingAddress.customAttributes['project']) {
                    if ($.isPlainObject(billingAddress.customAttributes['project'])) {
                        billingAddress['extension_attributes']['project'] = billingAddress.customAttributes['project'].value;
                    } else {
                        billingAddress['extension_attributes']['project'] = billingAddress.customAttributes['project'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }
                if (billingAddress.customAttributes['company_name']) {
                    if ($.isPlainObject(billingAddress.customAttributes['company_name'])) {
                        billingAddress['extension_attributes']['company_name'] = billingAddress.customAttributes['company_name'].value;
                    } else {
                        billingAddress['extension_attributes']['company_name'] = billingAddress.customAttributes['company_name'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }
                if (billingAddress.customAttributes != undefined) {
                    $.each(billingAddress.customAttributes, function (key, value) {

                        if ($.isPlainObject(value)) {
                            value = value['value'];
                            key = this.attribute_code;
                            if(value['attribute_code'])
                            {
                                key = value['attribute_code'];
                            }
                        }
                        billingAddress['extension_attributes'][key] = value;
                    });
                }

            }
            
            return originalAction(messageContainer);
        });
    };
});