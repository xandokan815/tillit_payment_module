define([
'mage/utils/wrapper',
'Tillit_Gateway/js/tillit'
], function(wrapper,Tillit) {
'use strict';

return function (createBillingAddressAction) {
    return wrapper.wrap(createBillingAddressAction, function(originalAction, addressData) {
 
    if (addressData.custom_attributes === undefined) {
        return originalAction(addressData);
    }

    if (addressData.custom_attributes['account_type'] != undefined) {
       
        addressData['custom_attributes']['account_type'] = addressData.custom_attributes['account_type'];
    }
    if (addressData.custom_attributes['company_id'] != undefined) {
        
        addressData['custom_attributes']['company_id'] = addressData.custom_attributes['company_id'];
    }

    if (addressData.custom_attributes['department'] != undefined) {
    
        addressData['custom_attributes']['department'] = addressData.custom_attributes['department'];
    }

    if (addressData.custom_attributes['project'] != undefined) {
        
        addressData['custom_attributes']['project'] = addressData.custom_attributes['project'];
    }

    if (addressData.custom_attributes['company_name'] != undefined) {
        
        addressData['custom_attributes']['company_name'] = addressData.custom_attributes['company_name'];
    }
    


    return originalAction(addressData);
    });
};
});
