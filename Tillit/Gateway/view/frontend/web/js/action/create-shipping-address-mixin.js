define([
'jquery',
'mage/utils/wrapper',
'Tillit_Gateway/js/tillit'
], function($, wrapper,Tillit) {
'use strict';

return function (createShippingAddressAction) {
    return wrapper.wrap(createShippingAddressAction, function(originalAction, addressData) {

    if (addressData.custom_attributes === undefined) {
        return originalAction();
    }
    if (addressData.custom_attributes['account_type'] != undefined) {
    
        addressData['custom_attributes']['account_type'] = addressData.custom_attributes['account_type'];
    }
    
    if (addressData.custom_attributes['compnay_id'] != undefined) {

        addressData['custom_attributes']['compnay_id'] = addressData.custom_attributes['compnay_id'];
    }

    if (addressData.custom_attributes['department'] != undefined) {
        addressData['custom_attributes']['department'] = addressData.custom_attributes['department'];
    }

    if (addressData.custom_attributes['project'] != undefined) {
        addressData['custom_attributes']['project'] = addressData.custom_attributes['project'];
    }

    if (addressData.custom_attributes['compnay_name'] != undefined) {
        addressData['custom_attributes']['compnay_name'] = addressData.custom_attributes['compnay_name'];
    }
    
    
    return originalAction();
    });
};
});