define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Tillit_Gateway/js/tillit'
  ], function($, wrapper, quote,Tillit) {
    'use strict';
        return function (setShippingInformationAction) {
            return wrapper.wrap(setShippingInformationAction, function(originalAction) {
        
                /* Tillit.populateFields();
                Tillit.ApproveOrder();       */          
                var shippingAddress = quote.shippingAddress();
        
                if(shippingAddress.customAttributes === undefined) {
                shippingAddress.customAttributes = {};
                }
        
                if(shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
                }
            
                try{
                if (shippingAddress.customAttributes['account_type']) {
                    if($.isPlainObject(shippingAddress.customAttributes['account_type'])){
                    shippingAddress['extension_attributes']['account_type'] = shippingAddress.customAttributes['account_type'].value;
                    } else{
                    shippingAddress['extension_attributes']['account_type'] = shippingAddress.customAttributes['account_type'];
                    }
                    //shippingAddress['extension_attributes']['account_type'] = shippingAddress.customAttributes['account_type'].value;
                }
        
                if (shippingAddress.customAttributes['company_id']) {
                    if($.isPlainObject(shippingAddress.customAttributes['company_id'])){
                    shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                    } else{
                    shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }

                if (shippingAddress.customAttributes['department']) {
                    if($.isPlainObject(shippingAddress.customAttributes['department'])){
                    shippingAddress['extension_attributes']['department'] = shippingAddress.customAttributes['department'].value;
                    } else{
                    shippingAddress['extension_attributes']['department'] = shippingAddress.customAttributes['department'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }
                if (shippingAddress.customAttributes['project']) {
                    if($.isPlainObject(shippingAddress.customAttributes['project'])){
                    shippingAddress['extension_attributes']['project'] = shippingAddress.customAttributes['project'].value;
                    } else{
                    shippingAddress['extension_attributes']['project'] = shippingAddress.customAttributes['project'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }
                if (shippingAddress.customAttributes['company_name']) {
                    if($.isPlainObject(shippingAddress.customAttributes['company_name'])){
                    shippingAddress['extension_attributes']['company_name'] = shippingAddress.customAttributes['company_name'].value;
                    } else{
                    shippingAddress['extension_attributes']['company_name'] = shippingAddress.customAttributes['company_name'];
                    }
                    //shippingAddress['extension_attributes']['company_id'] = shippingAddress.customAttributes['company_id'].value;
                }

                if (shippingAddress.customAttributes != undefined) {
                    $.each(shippingAddress.customAttributes, function (key, value) {
                        if($.isPlainObject(value)){
                            value = value['value'];
                            key = this.attribute_code;
                            if(value['attribute_code'])
                            {
                            key = value['attribute_code'];
                            }
        
                        }
                        shippingAddress['extension_attributes'][key] = value;
                    });
                }
                
                }catch (e) {
                return originalAction();
                }
                return originalAction();
            });
        };
    });