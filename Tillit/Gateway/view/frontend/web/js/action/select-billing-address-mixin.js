define([
    'mage/utils/wrapper',
    'Tillit_Gateway/js/tillit'
    ], function(wrapper,Tillit) {
    'use strict';
    
    return function (selectBillingAddressAction) {
        return wrapper.wrap(selectBillingAddressAction, function(originalAction) {
    
            
            originalAction();
            Tillit.populateFields();
            Tillit.ApproveOrder(false);
            return ;
        });
    };
    });
    