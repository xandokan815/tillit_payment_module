define([
    'jquery',
    'underscore',
    'mageUtils',
    'mage/translate',
    'Magento_Checkout/js/model/quote', 
    'Magento_Ui/js/model/messageList',
    'domReady!'
    ],
    function ($,_, utils, $t,quote,messageList) {
    'use strict';

    return (function () {
        const tillitRequiredField = '<abbr class="required" title="required">*</abbr>'
        const tillitSearchLimit = 50
        
        let tillitWithCompanySearch = null
        let tillitSearchCache
        let tillitMethodHidden = true
        let tillitApproved = null
        
        const tillitOrderIntentCheck = {
            "interval": null,
            "pendingCheck": false,
        }
        
        const tillitCompany = {
            "company_name": null,
            "country_prefix": null,
            "organization_number": null
        }
        const tillitRepresentative = {
            "email": null,
            "first_name": null,
            "last_name": null,
            "phone_number": null
        }
        var totals = quote.getTotals();
        return {
            setRepresentativeValue : function(key,value)
            {
                tillitRepresentative[key] = value;
            },
            setCompanyValue : function(key,value)
            {
                tillitCompany[key] = value;
            },
            settillitMethodHidden : function(value)
            {
                tillitMethodHidden = value;
            },
            gettillitApproved : function() {
                return tillitApproved;
            },
            gettillitMethodHidden : function() {
                return tillitMethodHidden;
            },
            getRepresentativeValue : function(key,value)
            {
                tillitRepresentative[key] = value;
            },
            getCompanyValue : function(key,value)
            {
                tillitCompany[key] = value;
            },
            canGetApproval : function()
            {
                if (window.enable_order_intent !== "1") {
                    return false
                }

                var shipping_address = (quote.billingAddress() != null)  ? quote.billingAddress() : quote.shippingAddress();
                var account_type = '';
                $.each(shipping_address.customAttributes, function (key, value) {
                    if($.isPlainObject(value)){
                        value = value['value'];
                        key = this.attribute_code;
                        if(value['attribute_code'])
                        {
                        key = value['attribute_code'];
                        }
    
                    }
                    if(key == 'account_type') {
                        account_type = value;
                    }
                });
                if(account_type == "personal")
                {
                    return false;
                }
                let can = true
                let values = [].concat(Object.values(tillitCompany), Object.values(tillitRepresentative))

                for(let i = 0; i < values.length; i++) {
                    const value = values[i]
                    if(!value || value.length === 0 || value === undefined || value === "undefined") {
                        can = false
                        break
                    }
                }

                return can

            },

            selectDefaultMethod : function (toggle_method = true)
            {
                // Get the Tillit payment method input
                const $tillitPaymentMethod = jQuery(':input[value="tillit_payment"]')

                // Get the Tillit payment block
                const $tillit = jQuery('div.payment-methods')

                // True if the Tillit payment method is disabled
                const isTillitDisabled = window.enable_order_intent === "1" && tillitMethodHidden === true

                // Disable the Tillit payment method for personal orders
                $tillitPaymentMethod.attr('disabled', isTillitDisabled)
                // If a personal account
                if(isTillitDisabled) {

                    if($tillit.length > 0)
                    {
                        if(toggle_method){
                        // Select the first visible payment method
                            $tillit.find('.payment-method').find('input[name="payment[method]"]').not(':input[value="tillit_payment"]').click();
                        }else{
                            $(".tillit-payment-method .checkout").attr('disabled', true);
                        }
                    }

                } else {
                    if(toggle_method) {
                            // Select the payment method for business accounts
                            $tillitPaymentMethod.click()
                            $(".tillit-payment-method .checkout").attr('disabled', false);
                    }else{
                        $(".tillit-payment-method .checkout").attr('disabled', false);
                    }
                }

            },


            toggleMethod : function (toggle_method = true)
            {

                // Get the Tillit payment method input
                const tillitPaymentMethod = $('input[value="tillit_payment"]')

                // True if the Tillit payment method is disabled
                const isTillitDisabled = window.enable_order_intent === '1' && tillitMethodHidden === true

                // Disable the Tillit payment method for personal orders
                tillitPaymentMethod.attr('disabled', isTillitDisabled)

                // Get the Tillit payment method
                const tillit = $('.tillit-payment-method');
                
                // If Tillit is disabled
                if(isTillitDisabled) {
                    // Get the next or previous target
                    if(tillit.length > 0)
                    {
                        const target = tillit.prev('.payment-method').length === 0 ? tillit.next('.payment-method') : tillit.prev('.payment-method')
                        //activate next default method
                        if(toggle_method)
                        {
                            
                            target.find('input[name="payment[method]"]').click();
                        }
                        else{
                            $(".tillit-payment-method .checkout").attr('disabled', true);
                        }
                    }
                    

                } else {
                    if(tillit.length > 0)
                    {
                        // Active the Tillit method
                        if(toggle_method)
                        {
                            
                            tillit.find('input[value="tillit_payment"]').click();
                            $(".tillit-payment-method .checkout").attr('disabled', false);
                        }
                        else{
                            $(".tillit-payment-method .checkout").attr('disabled', false);
                        }
                    }
                } 

            },
            populateFields : function() {
                var shipping_address = (quote.billingAddress() != null)  ? quote.billingAddress() : quote.shippingAddress();
                
                var company_id = '';
                $.each(shipping_address.customAttributes, function (key, value) {
                    if($.isPlainObject(value)){
                        value = value['value'];
                        key = this.attribute_code;
                        if(value['attribute_code'])
                        {
                        key = value['attribute_code'];
                        }
    
                    }
                    if(key == 'company_id') {
                        company_id = value;
                    }
                });
                var email = (quote.guestEmail) ? quote.guestEmail : window.checkoutConfig.customerData.email;
                tillitCompany.company_name = shipping_address.company
                tillitCompany.country_prefix = shipping_address.countryId
                tillitRepresentative.email = email
                tillitRepresentative.first_name = shipping_address.firstname
                tillitRepresentative.last_name = shipping_address.lastname
                tillitRepresentative.phone_number = shipping_address.telephone
                tillitCompany.organization_number = company_id;
            },
            ApproveOrder : function (toggle_method = true)
            {
                const canGetApprovals = this.canGetApproval();
                
                if(!canGetApprovals){
                    this.settillitMethodHidden(true);
                    this.toggleMethod(false);
                    return;
                } 

                if (tillitOrderIntentCheck.interval) {
                    tillitOrderIntentCheck.pendingCheck = true
                    return
                }
                var self = this;

                tillitOrderIntentCheck.interval = setInterval(function() {
                    if(!totals())
                    {
                        return;
                    }
                    let gross_amount = totals()['grand_total']
                    let tax_amount = totals()['tax_amount']
                    if (!gross_amount) {
                        return;
                    }
                    if (!tax_amount || tax_amount == "0.0000" || tax_amount == "0.000000") {
                        tax_amount = 0
                    }
                    clearInterval(tillitOrderIntentCheck.interval)
                    tillitOrderIntentCheck.interval = null
                    tillitOrderIntentCheck.pendingCheck = false
                    // Create an order intent
                    const approvalResponse = jQuery.ajax({
                        url: window.tillit_checkout_host + '/v1/order_intent',
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        method: 'POST',
                        headers: {
                            "Tillit-Merchant-Id": window.merchant_id
                        },
                        data: JSON.stringify({
                            "gross_amount": "" + gross_amount,
                            "buyer": {
                                "company": tillitCompany,
                                "representative": tillitRepresentative
                            },
                            "currency":totals()['base_currency_code'],
                            'merchant_id' : window.merchant_id,
                            "line_items": [{
                                "name": "Cart",
                                "description": "",
                                "gross_amount": gross_amount.toFixed(2),
                                "net_amount": (gross_amount - tax_amount).toFixed(2),
                                "discount_amount": "0",
                                "tax_amount": tax_amount.toFixed(2),
                                "tax_class_name": "VAT " + (100.0 * tax_amount / gross_amount).toFixed(2) + "%",
                                "tax_rate": "" + (1.0 * tax_amount / gross_amount).toFixed(6),
                                "unit_price": (gross_amount - tax_amount).toFixed(2),
                                "quantity": 1,
                                "quantity_unit": "item",
                                "image_url": "",
                                "product_page_url": "",
                                "type": "PHYSICAL",
                                "details": {
                                    "categories": [],
                                    "barcodes": []
                                },
                            }]
                        })
                    })

                    approvalResponse.done(function(response){
                    
                        // Store the approved state
                        tillitApproved = response.approved

                        // Toggle the Tillit payment method
                        tillitMethodHidden = !tillitApproved
                        // Show or hide the Tillit payment method
                        self.toggleMethod(toggle_method);

                        // Select the default payment method
                        self.selectDefaultMethod(toggle_method);

                        // Update company name in payment option
                    /*  if (document.querySelector('#select2-billing_company-container'))
                            document.querySelector('.tillit-buyer-name').innerText = document.querySelector('#select2-billing_company-container').innerText
                        else if (document.querySelector('#billing_company'))
                            document.querySelector('.tillit-buyer-name').innerText = document.querySelector('#billing_company').value */

                        // Update tillit message
                        messageList.addSuccessMessage({ message: window.subtitle_order_intent_ok });
                        let tillitSubtitleExistCheck = setInterval(function() {
                            if (document.querySelector('.tillit-subtitle')) {
                                document.querySelector('.tillit-subtitle').innerHTML = window.subtitle_order_intent_ok
                                clearInterval(tillitSubtitleExistCheck)
                            }
                        }, 1000)
                    })

                    approvalResponse.error(function(response){
                        // Store the approved state
                        tillitApproved = false

                        // Toggle the Tillit payment method
                        tillitMethodHidden = !tillitApproved

                        // Show or hide the Tillit payment method
                        self.toggleMethod(toggle_method);

                        // Select the default payment method
                        self.selectDefaultMethod(toggle_method);

                        // Update company name in payment option
                        //document.querySelector('.tillit-buyer-name').innerText = ''

                        // Display error messages
                        if (response.status >= 400) {
                            // @TODO: use code in checkout-api
                            let errMsg = (typeof response.responseJSON === 'string' || !('error_details' in response.responseJSON))
                                            ? response.responseJSON
                                            : response.responseJSON['error_details']

                            // Update tillit message
                            let tillitSubtitleExistCheck = setInterval(function() {
                                if (document.querySelector('.tillit-subtitle')) {
                                    document.querySelector('.tillit-subtitle').innerHTML = 
                                        errMsg.startsWith('Minimum Payment using Tillit') ? window.amount_min
                                        : errMsg.startsWith('Maximum Payment using Tillit') ? window.amount_max
                                        : window.subtitle_order_intent_reject
                                    
                                    clearInterval(tillitSubtitleExistCheck)
                                }
                            }, 1000)
                        } else {
                            let tillitSubtitleExistCheck = setInterval(function() {
                                if (document.querySelector('.tillit-subtitle')) {
                                    document.querySelector('.tillit-subtitle').innerHTML =window.subtitle_order_intent_reject
                                    clearInterval(tillitSubtitleExistCheck)
                                }
                            }, 1000)
                        }

                    })
                }, 1000)
            }
        }
    })();

});