var config = {
    config: {
        mixins: { 
            'Magento_Checkout/js/action/set-shipping-information': {
                'Tillit_Gateway/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/action/set-billing-address': {
                'Tillit_Gateway/js/action/set-billing-address-mixin': true
            },  
            'Magento_Checkout/js/action/select-billing-address': {
                'Tillit_Gateway/js/action/select-billing-address-mixin': true
            }, 
            'Magento_Checkout/js/action/select-shipping-address': {
                'Tillit_Gateway/js/action/select-shipping-address-mixin': true
            }, 
            'Magento_Checkout/js/action/create-shipping-address': {
                'Tillit_Gateway/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/create-billing-address': {
                'Tillit_Gateway/js/action/create-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'Tillit_Gateway/js/action/place-order-mixin': true
            }
        }
    }
};