<?php

namespace Tillit\Gateway\Plugin\Magento\Quote\Model;

class ShippingAddressManagement
{
    protected $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Tillit\Gateway\Helper\Config $helper 
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
    }

    public function beforeAssign(
        \Magento\Quote\Model\ShippingAddressManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\AddressInterface $address
    ) {

        $extAttributes = $address->getExtensionAttributes();

        if (!empty($extAttributes)) {

            try {
                $address->setAccountType($address->getExtensionAttributes()->getAccountType());
                $address->setCompanyId($address->getExtensionAttributes()->getCompanyId());           

                $address->setDepartment($address->getExtensionAttributes()->getDepartment());
                $address->setProject($address->getExtensionAttributes()->getProject());
                $address->setCompanyName($address->getExtensionAttributes()->getCompanyName());
            } catch (\Exception $e) {
                $address->setAccountType('');
                $address->setCompanyId('');
    
                $address->setDepartment('');
                $address->setProject('');
                $address->setCompanyName('');
                $this->logger->critical($e->getMessage());
            }

        } 
        else{
            $address->setAccountType('');
            $address->setCompanyId('');

            $address->setDepartment('');
            $address->setProject('');
            $address->setCompanyName('');
        }

        
    }
}