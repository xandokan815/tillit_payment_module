<?php

namespace Tillit\Gateway\Plugin\Checkout\Model;

class PaymentInformationManagement
{
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Tillit\Gateway\Helper\Config $helper 
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
    }
    public function beforeSavePaymentInformation(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        if (!$billingAddress) {
            return;
        }
        if ($billingAddress->getExtensionAttributes()) {
            $billingAddress->setAccountType($billingAddress->getExtensionAttributes()->getAccountType());
            $billingAddress->setCompanyId($billingAddress->getExtensionAttributes()->getCompanyId());

            $billingAddress->setDepartment($billingAddress->getExtensionAttributes()->getDepartment());
            $billingAddress->setProject($billingAddress->getExtensionAttributes()->getProject());
            $billingAddress->setCompanyName($billingAddress->getExtensionAttributes()->getCompanyName());
        } else {
            $billingAddress->setAccountType('');
            $billingAddress->setCompanyId('');


            $billingAddress->setDepartment('');
            $billingAddress->setProject('');
            $billingAddress->setCompanyName('');
        }
    }
}
