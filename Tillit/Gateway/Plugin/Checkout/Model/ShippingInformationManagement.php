<?php

namespace Tillit\Gateway\Plugin\Checkout\Model;

class ShippingInformationManagement
{
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Tillit\Gateway\Helper\Config $helper 
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();
        $billingAddress = $addressInformation->getBillingAddress();
        if ($shippingAddress->getExtensionAttributes()) {
            $shippingAddress->setAccountType($shippingAddress->getExtensionAttributes()->getAccountType());
            $shippingAddress->setCompanyId($shippingAddress->getExtensionAttributes()->getCompanyId());
        

            $shippingAddress->setDepartment($shippingAddress->getExtensionAttributes()->getDepartment());
            $shippingAddress->setProject($shippingAddress->getExtensionAttributes()->getProject());
            $shippingAddress->setCompanyName($shippingAddress->getExtensionAttributes()->getCompanyName());
        } else {
            $shippingAddress->setAccountType('');
            $shippingAddress->setCompanyId('');

            $shippingAddress->setDepartment('');
            $shippingAddress->setProject('');
            $shippingAddress->setCompanyName('');
            
        }

        if ($billingAddress->getExtensionAttributes()) {
            $billingAddress->setAccountType($billingAddress->getExtensionAttributes()->getAccountType());
            $billingAddress->setCompanyId($billingAddress->getExtensionAttributes()->getCompanyId());

            $billingAddress->setDepartment($billingAddress->getExtensionAttributes()->getDepartment());
            $billingAddress->setProject($billingAddress->getExtensionAttributes()->getProject());
            $billingAddress->setCompanyName($billingAddress->getExtensionAttributes()->getCompanyName());
        } else {
            $billingAddress->setAccountType('');
            $billingAddress->setCompanyId('');

            $billingAddress->setDepartment('');
            $billingAddress->setProject('');
            $billingAddress->setCompanyName('');
        }
    }
}
