<?php
namespace Tillit\Gateway\Plugin;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Tillit\Gateway\Helper\Config;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\Escaper;
use Magento\Framework\Mail\Template\TransportBuilder;

class ConfigPlugin
{
    protected $inlineTranslation;
    protected $escaper;
    protected $transportBuilder;
    protected $logger;


    public function __construct(
        RequestInterface $request,
        WriterInterface $configWriter,
        Config $helper,
        StateInterface $inlineTranslation,
        Escaper $escaper,
        TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager   
    ) {
        $this->request = $request;
        $this->configWriter = $configWriter;
        $this->helper = $helper;
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }
    
    public function aroundSave(
        \Magento\Config\Model\Config $subject,
        \Closure $proceed
    ) {
        $Params = $this->request->getParam('groups');
        if(isset($Params['tillit_payment']['fields']['merchant_logo']['value']) &&  isset($Params['tillit_payment']['fields']['tillit_merchant_id']['value'])){
            $logo = $Params['tillit_payment']['fields']['merchant_logo']['value']; 
            $merchant_id = $Params['tillit_payment']['fields']['tillit_merchant_id']['value'];
            $logo_path = $this->getMediaUrl('tillit/logo/'.$logo['value']);
            try {

                //send logo to merchant 
                $response = $this->helper->make_request("/v1/merchant/${merchant_id}/update", [
                    'merchant_id' => $merchant_id,
                    'logo_path' => $logo_path
                ]);
                
            } catch (\Exception $e) {
                
            }
        }
        
        // your custom logic
        return $proceed();
    }
    public function getMediaUrl($path) {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . $path;
    }
}