<?php


namespace Tillit\Gateway\Plugin\Quote\Model\Quote\Address;

class ToOrderAddress
{
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Tillit\Gateway\Helper\Config $helper 
    ) {
        $this->logger = $logger;
        $this->helper = $helper;
    }
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Address\ToOrderAddress $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Address $address,
        $data = []
    ) {
        $result = $proceed($address, $data);

        if ($address->getAccountType()) {
            $result->setAccountType($address->getAccountType());
        }
        else{
            $result->setAccountType('');
        }
        if ($address->getCompanyId()) {
            $result->setCompanyId($address->getCompanyId());
        }
        else{
            $result->setCompanyId('');
        }

        if ($address->getDepartment()) {
            $result->setDepartment($address->getDepartment());
        }
        else{
            $result->setDepartment('');
        }

        if ($address->getProject()) {
            $result->setProject($address->getProject());
        }
        else{
            $result->setProject('');
        }

        if ($address->getCompanyName()) {
            $result->setCompanyName($address->getCompanyName());
        }
        else{
            $result->setCompanyName('');
        }

        return $result;
    }
}
