<?php
namespace Tillit\Gateway\Plugin\Block\Widget\Button;

use Magento\Backend\Block\Widget\Button\Toolbar as ToolbarContext;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Backend\Block\Widget\Button\ButtonList;

class Toolbar
{
    /**
     * @param ToolbarContext $toolbar
     * @param AbstractBlock $context
     * @param ButtonList $buttonList
     * @return array
     */
    public function beforePushButtons(
        ToolbarContext $toolbar,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {
        if (!$context instanceof \Magento\Sales\Block\Adminhtml\Order\View) {
            return [$context, $buttonList];
        }

        $order = $context->getOrder();
        if (!$order) {
            return;
        }
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $methodCode = $method->getCode();
        if($methodCode === "tillit_payment" && $order->getState() === "pending_payment")
        {
            $buttonList->remove('order_hold');
            $buttonList->remove('void_payment');
            $buttonList->remove('accept_payment');
            $buttonList->remove('order_edit');
            $buttonList->remove('order_creditmemo');
            $buttonList->remove('order_invoice');
            $buttonList->remove('order_ship');
        }
        

        return [$context, $buttonList];
    }
}