<?php
namespace Tillit\Gateway\Controller\Payment;

use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Payment\Model\InfoInterface;

class Verificationfailed extends \Magento\Framework\App\Action\Action
{

    public function __construct(\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Tillit\Gateway\Model\Tillit $PaymentModel,
        \Tillit\Gateway\Helper\Config $helper ,
        \Magento\Framework\Message\Manager $MessageManager, 
        \Psr\Log\LoggerInterface $logger)
    {

        $this->_pageFactory = $resultPageFactory;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->PaymentModel = $PaymentModel;
        $this->_helper = $helper;
        $this->messageManager = $MessageManager;
        $this->logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        try{
            if($this->getRequest()->getParam('_tillit_order_reference') != '')
            {
                $order_reference = base64_decode($this->getRequest()->getParam('_tillit_order_reference'));

                $order = $this->_orderFactory->create()->getCollection()->addFieldToFilter('tillit_order_reference',$order_reference)->getFirstItem();

                if($order)
                {
                    $msg = __('Verification Failed');
                    $returnUrl = $this->_helper->getUrls('checkout/cart');
                    $this->_helper->orderFailed($order, $msg);       
                    $this->_helper->restoreQuote();
                    $this->messageManager->addError($msg); 

                }else{
                    throw new \Exception(__('Unable to find the requested order'));
                }

            }else {
                throw new \Exception(__('Unable to find the requested order'));
            }
            $this->orderRedirect($order, $returnUrl);
            return;
        }catch(\Exception $e)
        {
            $this->_helper->restoreQuote();
            $msg = 'Something went wrong while processing your order';
            $this->_helper->orderFailed($order, $msg);
            $this->messageManager->addError( $e->getMessage() );
            $returnUrl = $this->_helper->getUrls('checkout/cart');
            $this->orderRedirect($order, $returnUrl);
            return;
        }
        
    }

    public function orderRedirect($order, $returnUrl) {
        $this->getResponse()->setRedirect($returnUrl);
    }
    
}
?>