<?php
namespace Tillit\Gateway\Controller\Payment;

use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Payment\Model\InfoInterface;

class Cancel extends \Magento\Framework\App\Action\Action
{

    public function __construct(\Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Tillit\Gateway\Model\Tillit $PaymentModel,
        \Tillit\Gateway\Helper\Config $helper ,
        \Magento\Framework\Message\Manager $MessageManager, 
        \Psr\Log\LoggerInterface $logger)
    {

        $this->_pageFactory = $resultPageFactory;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        $this->PaymentModel = $PaymentModel;
        $this->_helper = $helper;
        $this->messageManager = $MessageManager;
        $this->logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        try{
            if($this->getRequest()->getParam('_tillit_order_reference') != '')
            {
                $order_reference = base64_decode($this->getRequest()->getParam('_tillit_order_reference'));

                $order = $this->_orderFactory->create()->getCollection()->addFieldToFilter('tillit_order_reference',$order_reference)->getFirstItem();

                if($order)
                {
                    $tillit_order_id = $order->getTillitOrderId();
                    
                    $response = $this->_helper->make_request("/v1/order/${tillit_order_id}/cancel");

                    $response = json_decode($response, true);
                    if(!isset($response)) {
                        $order->addStatusHistoryComment(sprintf(__('Could not update status to cancelled, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                        return $this;
                    }
            
                    $tillit_err = $this->_helper->get_tillit_error_msg($response);
                    if ($tillit_err) {
                        $order->addStatusHistoryComment(sprintf(__('Could not update status to cancelled, please check with Tillit admin for id %s'), $tillit_order_id))->save();
                        return $this;
                    }

                    $order->addStatusHistoryComment(sprintf(__('Tillit Order marked as cancelled')))->save();

                    $msg = __('Cancelled Order');
                    $returnUrl = $this->_helper->getUrls('checkout/cart');
                    $this->_helper->orderFailed($order, $msg);       
                    $this->_helper->restoreQuote();
                    $this->messageManager->addError($msg); 

                }else{
                    throw new \Exception(__('Unable to find the requested order'));
                }

            }else {
                throw new \Exception(__('Unable to find the requested order'));
            }
            $this->orderRedirect($order, $returnUrl);
            return;
        }catch(\Exception $e)
        {
            $this->_helper->restoreQuote();
            $msg = 'Something went wrong while processing your order';
            $this->_helper->orderFailed($order, $msg);
            $this->messageManager->addError( $e->getMessage() );
            $returnUrl = $this->_helper->getUrls('checkout/cart');
            $this->orderRedirect($order, $returnUrl);
            return;
        }
        
    }

    public function orderRedirect($order, $returnUrl) {
        $this->getResponse()->setRedirect($returnUrl);
    }
    
}
?>